import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class JwtinterceptorInterceptor implements HttpInterceptor {

  constructor(private _router: Router) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const newToken = localStorage.getItem('token');

    if (newToken) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${newToken}`,
        },
      });
    }
   
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          // El servidor respondió con un código 401 (No autorizado), lo que indica que el token ha expirado o no es válido.
          // Aquí puedes redirigir al usuario al inicio de sesión.
          this._router.navigate(['/login']);
        }
        return throwError(error);
      })
    );
  }
}

