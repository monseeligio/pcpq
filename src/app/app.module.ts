import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { MenuComponent } from './components/menu/menu.component';
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http';
import { JwtinterceptorInterceptor } from './jwtinterceptor.interceptor';

import { NavbarComponent } from './components/navbar/navbar.component';
import { NuevoUsuarioComponent } from './components/nuevo-usuario/nuevo-usuario.component';
import { FooterComponent } from './components/footer/footer.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { CotizacionComponent } from './components/cotizacion/cotizacion.component';
import { ProductosComponent } from './components/desecantes/desecantes.component';
import { HotmeltComponent } from './components/hotmelt/hotmelt.component';
import { LiquidosComponent } from './components/liquidos/liquidos.component';
import { PlastisolComponent } from './components/plastisol/plastisol.component';
import { VciComponent } from './components/vci/vci.component';
import { Cotizacion2Component } from './components/cotizacion2/cotizacion2.component';
import { CotizacionesComponent } from './components/cotizaciones/cotizaciones.component';
import { GuiaDesecantesComponent } from './components/guia-desecantes/guia-desecantes.component';
import { GuiaPComponent } from './components/guia-p/guia-p.component';
import { GuiaLComponent } from './components/guia-l/guia-l.component';
import { GuiaHComponent } from './components/guia-h/guia-h.component';
import { GuiaVComponent } from './components/guia-v/guia-v.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { VerclientesComponent } from './components/verclientes/verclientes.component';
import { AprobacionesComponent } from './components/aprobaciones/aprobaciones.component';
import { AprobacionCotiComponent } from './components/aprobacion-coti/aprobacion-coti.component';
import { VisuliazarcotiComponent } from './components/visuliazarcoti/visuliazarcoti.component';
import { CotizacionusuarioComponent } from './components/cotizacionusuario/cotizacionusuario.component';
import { ProductosGComponent } from './components/productos-g/productos-g.component';
import { ProductonuevoComponent } from './components/productonuevo/productonuevo.component';
import { ReporteComponent } from './components/reporte/reporte.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuComponent,
    NavbarComponent,
    NuevoUsuarioComponent,
    FooterComponent,
    UsuarioComponent,
    CotizacionComponent,
    ProductosComponent,
    HotmeltComponent,
    LiquidosComponent,
    PlastisolComponent,
    VciComponent,
    Cotizacion2Component,
    CotizacionesComponent,
    GuiaDesecantesComponent,
    GuiaPComponent,
    GuiaLComponent,
    GuiaHComponent,
    GuiaVComponent,
    ClientesComponent,
    VerclientesComponent,
    AprobacionesComponent,
    AprobacionCotiComponent,
    VisuliazarcotiComponent,
    CotizacionusuarioComponent,
    ProductosGComponent,
    ProductonuevoComponent,
    ReporteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,

  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtinterceptorInterceptor, // Agrega aquí tu interceptor
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
