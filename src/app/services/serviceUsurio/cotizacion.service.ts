import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { URL} from '../../config/config';


@Injectable({
  providedIn: 'root'
})
export class CotizacionService {

  numeroCotizacionReal: string = '';
  public url;
  public datosCotizacionAprobar:any


  constructor(private _http:HttpClient) { 
    this.url = URL;
  }

  setNumeroCotizacionReal(numero: string) {
    this.numeroCotizacionReal = numero;
  }

  getNumeroCotizacionReal() {
    return this.numeroCotizacionReal;
  }
  
    datosCotizacion: any = {
      FechaEmision: '',
      FechaExpiracion: '',
      Total: '',
      Clientes_ID_Cliente: '',
      Usuario_Clave_usuario: '',
      Comentarios: '',
      Status: ''
    };
  
    productos: any[] = [];
  

    agregarProducto(producto: any) {
      this.productos.push(producto);
      alert("Agregado")
    }
  
  
    obtenerProductos() {
      return this.productos;
    }

    crearCotizacionYProductos(body:any) {
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      const url = `${this.url}/nuevo/ProdutoYcotizacion`;
      return this._http.post(url, body, { headers });
    }
    
    
    enviarID(item:any){
      this.datosCotizacionAprobar=item
      
    }

    
    ObtenerID(){      
      return this.datosCotizacionAprobar;
    }

    obtenerNotificaciones(clave:any){
      return this._http.get(`${this.url}/notificaciones/${clave}`);
    }

  


}
