import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { URL } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  public url;
  constructor(private _http:HttpClient) { 
    this.url = URL;
   
  }

  login(body:any) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.url}/login`;
    return this._http.post(url, body, { headers });
  }

  
  agregarUsuario(body:any) {
    const url = `${this.url}/nuevoRol`;
    //console.log(body);
    return this._http.post(url, body);
  }

  getRol() {
    const url = `${this.url}/roles`;
    return this._http.get(url);
  }



  DeleteRol(id:any) {
    const url = `${this.url}/delete/rol/${id}`;
    return this._http.delete(url,);
  }

  UpdateRol(id:any) {
    const url = `${this.url}/update/rol/${id}`;
    return this._http.put(url, id, );
  }


  buscarUsuario(body:any) {
    const url = `${this.url}/get/usuario/${body}`;
    return this._http.get(url);
  }

  
  vendedores() {
    const url = `${this.url}/vendedores`;
    return this._http.get(url);
  }
  gerentes(){
      const url = `${this.url}/gerentes`;
      return this._http.get(url);
    
  }

  
  clientes() {
    const url = `${this.url}/clientes`;
    return this._http.get(url);
}
  


  clientesC() {
    const url = `${this.url}/contar/clientes`;
    return this._http.get(url);
  }
  ProductosContar() {
    const url = `${this.url}/contar/productos`;
    return this._http.get(url);
  }
  CotizacionesContar() {
    const url = `${this.url}/contar/cotizaciones`;
    return this._http.get(url);
  }
  CotizacionesAprobarContar() {
    const url = `${this.url}/contar/cotizaciones-aprobar`;
    return this._http.get(url);
  }



  obtenerProductosCotizacidos(){
      const url = `${this.url}/productoVendidos`;
      return this._http.get(url);
  }
  
  
  Productos() {
    const url = `${this.url}/productos`;
    return this._http.get(url);
  }

  ProductosD() {
    const url = `${this.url}/productoD`;
    return this._http.get(url);
  }

  ProductosH() {
    const url = `${this.url}/productoH`;
    return this._http.get(url);
  }
  ProductosL() {
    const url = `${this.url}/productoL`;
    return this._http.get(url);
  }
  ProductosP() {
    const url = `${this.url}/productoP`;
    return this._http.get(url);
  }
  ProductosV() {
    const url = `${this.url}/productoV`;
    return this._http.get(url);
  }

  
  getImagen(id:any) {
    const url = `${this.url}/getImage/${id}`;
    return url
  }

  agregarCotizacion(body:any) {
    const url = `${this.url}/nueva/cotizacion`;
    //console.log(body);
    return this._http.post(url, body);
  }

  agregarProductosCotizacion(body:any) {
    const url = `${this.url}/nuevo/Produto/cotizacion`;
    //console.log(body);
    return this._http.post(url, body);
  }

  obtenerNumCotizacion(){
    const url = `${this.url}/next`;
    return this._http.get(url);
  }

  obtenerProductosCotizados(id:any) {
    const url = `${this.url}/obtener/productos/${id}`;
    return this._http.get(url);
  }

  eliminarProductoCotizado(id:any) {
    const url = `${this.url}/eliminar/productos/${id}`;
    return this._http.delete(url);
  }

  obtenerCotizaciones() {
    const url = `${this.url}/cotizaciones`;
    return this._http.get(url);
  }
  
  obtenerNumeroCotizaciones() {
    const url = `${this.url}/contar/cotizaciones`;
    return this._http.get(url);
  }

  ventaguiadaD(opcionesSeleccionadas: string[]){
    const url = `${this.url}/venta/guiada/desecantes`;
    return this._http.post(url, { opcionesSeleccionadas });
  }

  ventaguiadaL(opcionesSeleccionadas: string[]){
    const url = `${this.url}/venta/guiada/liquidos`;
    return this._http.post(url, { opcionesSeleccionadas });
  }

  ventaguiadaH(opcionesSeleccionadas: string[]){
    const url = `${this.url}/venta/guiada/hotmelt`;
    return this._http.post(url, { opcionesSeleccionadas });
  }
  ventaguiadaP(opcionesSeleccionadas: string[]){
    const url = `${this.url}/venta/guiada/plastisol`;
    return this._http.post(url, { opcionesSeleccionadas });
  }

  ventaguiadaV(opcionesSeleccionadas: string[]){
    const url = `${this.url}/venta/guiada/vci`;
    return this._http.post(url, { opcionesSeleccionadas });
  }

  agregarCliente(body:any) {
    const url = `${this.url}/nuevo-cliente`;
    //console.log(body);
    return this._http.post(url, body);
  }

  getCliente() {
    const url = `${this.url}/todos-clientes`;
    return this._http.get(url);
  }

  DeleteCliente(id:any) {
    const url = `${this.url}/delete/cliente/${id}`;
    return this._http.delete(url);
  }

  UpdateCliente(id:any) {
    const url = `${this.url}/update/cliente/${id}`;
    return this._http.put(url, id);
  }

  buscarCliente(body:any) {
    const url = `${this.url}/get/cliente/${body}`;
    return this._http.get(url);
  }

  
  
  obtenerCotizacionesRevisar(clave:any) {
    const url = `${this.url}/cotizaciones-aprobar/${clave}`;
    return this._http.get(url);
  }


  getCotiUsuario(id:any) {
    const url = `${this.url}/cotizacion-usuario/${id}`;
    return this._http.get(url);
  }


  
  obtenerdatosCoti(id:any) {
    const url = `${this.url}/Datos-cotizacion-aprobar/${id}`;
    return this._http.get(url, id);
  }

  obtenerProductosCotiA(id:any) {
    const url = `${this.url}/productos-cotizacion-aprobar/${id}`;
    return this._http.get(url, id);
  }


  aprobarCotizacion(body:any){    
    const url = `${this.url}/aprobacion`;
    return this._http.put(url, body);
  }

  rechazarCotizacion(body:any){    
    const url = `${this.url}/cotizacion/rechazada`;
    return this._http.put(url, body);
  }
  

  EnviarAprobacion(body:any) {
    const url = `${this.url}/enviar/aprobacion`;
    //console.log(body);
    return this._http.post(url, body);
  }

  saveData(formData: FormData){
    const url = `${this.url}/api/upload`;
    return this._http.post(url, formData);
  }

  categorias() {
    const url = `${this.url}/categorias`;
    return this._http.get(url);
  }

  agregarProducto(body:any) {
    const url = `${this.url}/nuevo-producto`;
    //console.log(body);
    return this._http.post(url, body);
  }

  crearCotizacionYProductos(body:any) {
    const url = `${this.url}/nuevo/ProdutoYcotizacion`;
    console.log(body);
    return this._http.post(url, body);
  }

}
