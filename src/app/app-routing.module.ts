import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './components/menu/menu.component';
import { LoginComponent } from './components/login/login.component';
import { NuevoUsuarioComponent } from './components/nuevo-usuario/nuevo-usuario.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { CotizacionComponent } from './components/cotizacion/cotizacion.component';
import { ProductosComponent } from './components/desecantes/desecantes.component';
import { HotmeltComponent } from './components/hotmelt/hotmelt.component';
import { PlastisolComponent } from './components/plastisol/plastisol.component';
import { LiquidosComponent } from './components/liquidos/liquidos.component';
import { VciComponent } from './components/vci/vci.component';
import { Cotizacion2Component } from './components/cotizacion2/cotizacion2.component';
import { CotizacionesComponent } from './components/cotizaciones/cotizaciones.component';
import { GuiaDesecantesComponent } from './components/guia-desecantes/guia-desecantes.component';
import { GuiaPComponent } from './components/guia-p/guia-p.component';
import { GuiaLComponent } from './components/guia-l/guia-l.component';
import { GuiaHComponent } from './components/guia-h/guia-h.component';
import { GuiaVComponent } from './components/guia-v/guia-v.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { VerclientesComponent } from './components/verclientes/verclientes.component';
import { AprobacionesComponent } from './components/aprobaciones/aprobaciones.component';
import { AprobacionCotiComponent } from './components/aprobacion-coti/aprobacion-coti.component';
import { VisuliazarcotiComponent } from './components/visuliazarcoti/visuliazarcoti.component';
import { CotizacionusuarioComponent } from './components/cotizacionusuario/cotizacionusuario.component';
import { ProductosGComponent } from './components/productos-g/productos-g.component';
import { ProductonuevoComponent } from './components/productonuevo/productonuevo.component';
import { ReporteComponent } from './components/reporte/reporte.component';

const routes: Routes = [
  {path: '', pathMatch:'full' ,redirectTo:'login'},
  {path: 'menu', component: MenuComponent },
  {path: 'login', component: LoginComponent },
  {path: 'nuevoUsuario', component: NuevoUsuarioComponent },
  {path: 'usuarios', component: UsuarioComponent },
  {path: 'cotizacion', component: Cotizacion2Component},
  {path: 'desecantes', component: ProductosComponent },
  {path: 'hotmelt', component: HotmeltComponent },
  {path: 'liquidos', component: LiquidosComponent },
  {path: 'plastisol', component: PlastisolComponent },
  {path: 'vci', component: VciComponent },
  {path: 'cotizaciones', component: CotizacionesComponent },
  {path: 'cotizacionDolares', component: CotizacionComponent},
  {path: 'venta-guiada-desecantes', component: GuiaDesecantesComponent},
  {path: 'venta-guiada-plastisol', component: GuiaPComponent},
  {path: 'venta-guiada-liquidos', component: GuiaLComponent},
  {path: 'venta-guiada-hotmelt', component: GuiaHComponent},
  {path: 'venta-guiada-vci', component: GuiaVComponent},
  {path: 'nuevo-cliente', component: ClientesComponent},
  {path: 'ver-cliente', component: VerclientesComponent},
  {path: 'aprobaciones', component: AprobacionesComponent},
  {path: 'aprobar-cotizacion', component: AprobacionCotiComponent},
  {path: 'visualizar-cotizacion', component: VisuliazarcotiComponent},
  {path: 'cotizacion-usuario', component: CotizacionusuarioComponent},
  {path: 'productos', component: ProductosGComponent},
  {path: 'nuevo-producto', component: ProductonuevoComponent},
  {path: 'reporte', component: ReporteComponent},




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
