import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { URL, URL2 } from '../../config/config';




@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent {

  @ViewChild("update", {static: false}) myModalInfo!: TemplateRef<any>; 


  public datos:any;
  public status:any;

  datosUsuario={Clave_usuario:'', Nombre:'', ApellidoP:'', ApellidoM:'', Rol:'', email:'', Status:'', Password:'', Telefono:'', imagen:''}
  datosUsuarioU={Clave_usuario:'', Nombre:'', ApellidoP:'', ApellidoM:'', Rol:'', email:'', Status:'', Password:'', Telefono:'', imagen:''}

  public imagen:any
  public buscarUsuario:any
  public confirmarContra:any
  
  constructor(private _serviceM:UsuarioService, private modalService: NgbModal){}
  
  ngOnInit(): void {
    this.getRol2()
  }

  mostrarModalInfo(id:any){
  this.modalService.open(this.myModalInfo);
  }

  
  
  mostrarConfirmacionEliminar1(id:any): Promise<boolean> {
    return Swal.fire({
      title: '¿Estás seguro?',
      text: 'No podrás revertir esto',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, borrarlo'
    }).then((result) => {
      if (result.isConfirmed) {
        this.EliminarRol(id);
      }
      return result.isConfirmed;
    });
  }
  public urlImagen:any
  public rutaImagen:any
  public contrasena:any
  mostrarModalUptade1(id:any){
    this.modalService.open(this.myModalInfo);
    this.datosUsuarioU.Clave_usuario=id.Clave_usuario
    this.datosUsuarioU.Nombre=id.Nombre
    this.datosUsuarioU.ApellidoP=id.ApellidoP
    this.datosUsuarioU.ApellidoM=id.ApellidoM
    this.datosUsuarioU.Rol=id.Rol 
    this.datosUsuarioU.email=id.email
    this.datosUsuarioU.Telefono=id.Telefono
    this.rutaImagen = id.imagen 
    this.urlImagen = `${URL2}${this.rutaImagen}`;
    //console.log(id.Status);

    if (id.Status=='Activo') {
        this.status='1'
    }
    if (id.Status=='Inactivo') {
      this.status='0'
  }
    this.datosUsuarioU.Status=this.status;    
  }


  getRol(){
    this._serviceM.getRol().subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.datos = resp;        
    }
  })
  }



  
  onImageSelect(event: any) {
    const file = event.target.files[0];
    this.datosUsuarioU.imagen = file;
  }

  getRol2() {
    this._serviceM.getRol().subscribe((resp: any) => {
      if (Array.isArray(resp)) {
        this.datos = resp.map((rol: any) => {
          // Convertir el valor booleano a "Activo" o "Inactivo"
          rol.Status = rol.Status === 1 ? "Activo" : "Inactivo";
          return rol;
        });
  
        this.datos.reverse(); // Invierte el orden del arreglo
        //console.log(resp);
      }
    });
    this.buscarUsuario=''
  }
  
  BuscarUsuario(){
    this._serviceM.buscarUsuario(this.buscarUsuario).subscribe((resp:any)=>{
      if (Array.isArray(resp)) { 
        this.datos = resp.map((rol: any) => {
          // Convertir el valor booleano a "Activo" o "Inactivo"
          rol.Status = rol.Status === 1 ? "Activo" : "Inactivo";
          return rol;
        });
  
        this.datos.reverse(); // Invierte el orden del arreglo
        //console.log(resp);      
    }
  })
  }


  EliminarRol(id: string){    
    this._serviceM.DeleteRol(id).subscribe((data:any)=>{
      //console.log(id);
      this.getRol2();
    }) 
  }


  AgregarUser(){
    if(this.datosUsuario.Clave_usuario=='' ){
      //COMPRUEBA SI EXISTEN CAMPOS VACIOS
      window.alert("Debes ingresar todos los datos solicitados")
    }
    else{
      //console.log(this.datosUsuario);//DATOS REGISTRADOS MOSTRADOS EN CONSOLA
    this._serviceM.agregarUsuario(this.datosUsuario).subscribe((data:any)=>{//LLAMA AL SERVICIO PARA AGREGAR AL USUARIO
      if (data ['ok']) {//SI LOS DATOS ESTAN COMPLETOS LO REGISTRA
        alert("Registrado exitosamente");
      } else
      alert("bein");//EN CASO CONTRARIO MANDA UN ERROR
    })}
  }

  mostrarConfirmacionUpdate(): Promise<boolean> {
    return Swal.fire({
      title: '¿Estás seguro?',
      text: 'Dar clic en si para actualizar registro',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, Actualizar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.actualizar();
      }
      return result.isConfirmed;
    });
  }

  actualizar(){
    const formData = new FormData();
    if(this.datosUsuarioU.Nombre=='' || this.datosUsuarioU.ApellidoP=='' || this.datosUsuarioU.email=='' || this.datosUsuarioU.email=='' || this.datosUsuarioU.Telefono=='' || this.datosUsuarioU.Status=='' || this.datosUsuarioU.Rol==''){
      alert("Debes ingresar los datos solicitados")
    }
    else{
      formData.append('Clave_usuario', this.datosUsuarioU.Clave_usuario);
      formData.append('Nombre', this.datosUsuarioU.Nombre);
      formData.append('ApellidoP', this.datosUsuarioU.ApellidoP);
      formData.append('ApellidoM', this.datosUsuarioU.ApellidoM);
      formData.append('Rol', this.datosUsuarioU.Rol);
      formData.append('email', this.datosUsuarioU.email);
      formData.append('Status', this.datosUsuarioU.Status);
      formData.append('Password', this.datosUsuarioU.Password);
      formData.append('Telefono', this.datosUsuarioU.Telefono);
      
      if (this.datosUsuarioU.imagen == '') {
        this.datosUsuarioU.imagen = this.rutaImagen;
        formData.append('imagen', this.datosUsuarioU.imagen);
      } 
      else {
        formData.append('imagen', this.datosUsuarioU.imagen);
      } 
      this._serviceM.UpdateRol(formData).subscribe((data:any)=>{
      if (data.message == 'Datos actualizados exitosamente') {
        alert("Datos actualizados exitosamente");
      } 
      else if(data.mensaje == 'Debes ingresar correctamente los datos solicitados') {
        alert("La contraseña debe contener al menos 8 caracteres");
      }
      this.datosUsuarioU={Clave_usuario:'', Nombre:'', ApellidoP:'', ApellidoM:'', Rol:'', email:'', Status:'', Password:'', Telefono:'', imagen:''}
      this.confirmarContra=''
      this.getRol2()
      this.modalService.dismissAll()
    })
    }
  }
    cerrar(){
      this.modalService.dismissAll()
    }



}
