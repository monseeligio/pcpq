import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisuliazarcotiComponent } from './visuliazarcoti.component';

describe('VisuliazarcotiComponent', () => {
  let component: VisuliazarcotiComponent;
  let fixture: ComponentFixture<VisuliazarcotiComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VisuliazarcotiComponent]
    });
    fixture = TestBed.createComponent(VisuliazarcotiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
