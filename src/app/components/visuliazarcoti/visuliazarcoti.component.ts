import { Component, ViewChild, TemplateRef } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import { CotizacionService } from 'src/app/services/serviceUsurio/cotizacion.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import jsPDF from 'jspdf';
import { format } from 'date-fns';
import autoTable from 'jspdf-autotable'
import { URL, URL2 } from '../../config/config';



@Component({
  selector: 'app-visuliazarcoti',
  templateUrl: './visuliazarcoti.component.html',
  styleUrls: ['./visuliazarcoti.component.css']
})
export class VisuliazarcotiComponent {
  @ViewChild("pdf", {static: false}) myModalPDF!: TemplateRef<any>; 

  constructor(private _service:UsuarioService, private productosService:CotizacionService, private _router: Router, private modalService: NgbModal){}
  ngOnInit(): void {
    this.obtener()
    this.obtenerProductos()
  }

  mostrarModalPDF(){
    this.modalService.open(this.myModalPDF);
  }
  
  datosCotizacion={FechaEmision:'', FechaExpiracion:'', Total:'', Flete:'',Clientes_ID_Cliente:'', Usuario_Clave_usuario:'', Comentarios:'', Status:''}
  public datos:any
  public NumCotizacion:any
  public datos2:any
  public productos:any
  public Clave_usuario:any
  datosUsuarioU={Clave_usuario:'', Nombre:'', ApellidoP:'', ApellidoM:'', Rol:'', email:'', Telefono:'', imagen:''}
  public idioma:any

  obtener() {
    this.datos = this.productosService.ObtenerID();    
    this._service.obtenerdatosCoti(this.datos).subscribe((resp: any) => {
      this.datosCotizacion.FechaEmision = resp[0].FechaEmision;
      const fecha = new Date(resp[0].FechaEmision);
      const fechaFormateada = format(fecha, 'dd/MM/yy');
      this.datosCotizacion.FechaEmision=fechaFormateada;
      const fecha2 = new Date(resp[0].FechaExpiracion);
      const fechaFormateada2 = format(fecha2, 'dd/MM/yy');
      this.datosCotizacion.FechaExpiracion = fechaFormateada2;
      this.datosCotizacion.Total = resp[0].Total;
      this.datosCotizacion.Flete = resp[0].Flete;
      this.datosCotizacion.Clientes_ID_Cliente = resp[0].Razon_Social;
      this.datosCotizacion.Usuario_Clave_usuario = resp[0].Nombre;
      this.Clave_usuario= resp[0].Clave_usuario;
      this.datosCotizacion.Comentarios = resp[0].Comentarios;
      this.datosCotizacion.Status = resp[0].Status;
      this.datosUsuarioU.Nombre = resp[0].Nombre;
      this.datosUsuarioU.ApellidoP = resp[0].ApellidoP;
      this.datosUsuarioU.Rol = resp[0].Rol;
      this.datosUsuarioU.email = resp[0].email;
      this.datosUsuarioU.Telefono = resp[0].Telefono;
      this.datosUsuarioU.imagen = resp[0].imagen;





      //console.log(resp);
     
    });
  }

  obtenerProductos(){
    this.datos2=this.productosService.ObtenerID()
    this._service.obtenerProductosCotiA(this.datos2).subscribe((resp: any) => {
      this.productos=resp
      //console.log("obtiene productos",this.productos);
    })
  }

  GenerarPDF(){   
    //console.log(this.datosCotizacion.Status);
    if(this.datosCotizacion.Status=='Revisar' || this.datosCotizacion.Status=='Rechazada'){
      alert("El estado de tu cotización debe ser Aprobada")
    }
    else if(this.datosCotizacion.Status=="Aprobada" ){
    const doc = new jsPDF({ unit: 'mm', format: [215.9, 279.4] }); // Tamaño personalizado en milímetros (215.9 mm x 279.4 mm)
    doc.setFontSize(9); 
    doc.rect(167.30, 2.76, 45.95, 21);
    doc.setFont("Helvetica", "bold");
    doc.text('Código: FO06-VEN-01', 173, 6.26);
    doc.text('Emisión: 03/03/2023', 173, 10.16);
    doc.text('Revisión: 01', 173, 14.94);
    doc.text('Página: 1 de 4', 173, 19.24);
    doc.rect(167.30, 2.76, 4, 21, 'F'); // 'F' indica que el rectángulo debe ser relleno


    const imgData =  "assets/img/ImgCoti/persona.jpg";
    //x,y,ancho, alto
    doc.addImage(imgData, 'JPEG',28.6, 60, 161 , 172); 

    doc.setFontSize(39.5); 
    doc.text('PROPUESTA COMERCIAL', 17.66, 43);
    doc.rect(0, 184.18, 215.9, 95, 'F');

    const imgData2 =  "assets/img/ImgCoti/logo.png";
    doc.addImage(imgData2, 'PNG', 85.17, 214.20, 44.7 , 41.7);

    doc.setFontSize(13); 
    doc.setFont("Helvetica", "bold");
    doc.setTextColor(255, 255, 255);
    doc.text('Contáctanos', 32.06, 234.7);
    doc.setFontSize(12); 
    doc.text('soporte@parautos.com.mx', 16.08, 244.3);
    doc.text('7775006144', 16.08, 254.12);
    doc.text('www.parautos.com.mx', 16.08, 263.8);

    doc.setFontSize(13); 
    doc.text('TAKE THE WORK OUT', 149.4, 251.6);
    doc.text('OF YOUR WORKDAY', 149.4, 259.05);

    doc.addPage();
    doc.rect(0, 0, 21.94, 279.4, 'F'); // 'F' indica que el rectángulo debe ser relleno
    doc.setTextColor(0, 0, 0);
    doc.setFontSize(45.5); 
    doc.text('Nosotros', 73.58, 24.32);
    doc.setFontSize(12); 
    doc.text('Durante más de 20 años, seguimos enorgulleciéndonos de brindar atención', 34.54, 35.68);
    doc.text('y apoyo a nuestros clientes, quienes gozan de beneficios EXCLUSIVOS tales', 34.54, 42.95);
    doc.text('como: Educación digital, Soporte gratuito y diagnóstico empresarial.', 41.56, 49.18);

    doc.text('Contamos con +7 líneas de negocio cuidadosamente seleccionadas con el', 34.54, 60);
    doc.text('fin de aportar a tu empresa beneficios y soluciones exclusivas.', 47.11, 66.55);

    const anticorro =  "assets/img/ImgCoti/anticorrosivos.jpg";
    doc.addImage(anticorro, 'JPEG',46.76, 76.55, 61.65 , 60.96); 
    doc.setFontSize(20); 

    const backgroundColor = '#FFFFFF'; 
    doc.setFillColor(backgroundColor);
    doc.rect(45.46, 104.12, 70.68, 9.16, 'F');
    doc.text('ANTICORROSIVOS', 45.76, 112);


    const lubri =  "assets/img/ImgCoti/lubricantes.jpg";
    doc.addImage(lubri, 'JPEG',116.69, 76.55, 61.65 , 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(116.69, 104.12, 70.68, 9.16, 'F');
    doc.text('LUBRICANTES', 120.69, 112);
    

    const liq =  "assets/img/ImgCoti/liquidoM.jpg";
    doc.addImage(liq, 'JPEG',46.76, 145.32, 61.65 , 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(46.76, 173.7, 70.68, 9.16, 'F');
    doc.text('LIQ. MAQUINADO', 47.69, 181.3);
    

    const hotmelt =  "assets/img/ImgCoti/Hotmelts.jpg";
    doc.addImage(hotmelt, 'JPEG',116.69, 145.32, 61.65 , 60.96); 
    doc.setFillColor(backgroundColor);
    doc.rect(116.69, 173.7, 70.68, 9.16, 'F');
    doc.text('HOTMELTS', 126.69, 181.3);


    const pla =  "assets/img/ImgCoti/plastisol.jpg";
    doc.addImage(pla, 'JPEG',46.76, 213.08, 61.65 , 60.96); 
    doc.setFillColor(backgroundColor);
    doc.rect(46.76, 244.75, 70.68, 9.16, 'F');
    doc.text('PLASTISOLES', 51.12, 252.75);

    const erp =  "assets/img/ImgCoti/ERP.jpg";
    doc.addImage(erp, 'JPEG',116.69, 213.08, 61.65 , 60.96); 
    doc.setFillColor(backgroundColor);
    doc.rect(116.69, 244.75, 70.68, 9.16, 'F');
    doc.text('ERP', 138.19, 252.75);

    doc.addPage();
    doc.setFontSize(45.5); 
    doc.text('Cotización', 28.16, 26.46);
    const backgroundColorN = '#393939'; 
    doc.setFillColor(backgroundColorN);


    const Logo2 =  "assets/img/ImgCoti/parautosN.jpeg";
    doc.addImage(Logo2, 'JPEG',184.11, 6, 27.77 , 26.77); 
    doc.rect(28.91, 46.75, 46.36, 23.44);
    doc.rect(28.91, 46.75, 3.98, 23.44, 'F');

    doc.rect(84.14, 46.75, 46.36, 23.44);
    doc.rect(84.14, 46.75, 3.98, 23.44, 'F');

    doc.rect(140.15, 46.75, 46.36, 23.44);
    doc.rect(140.15, 46.75, 3.98, 23.44, 'F');

    doc.setFontSize(10.5); 
    doc.text('Cotización', 38.12, 51.61);
    doc.text('Fecha', 104.40, 51.61);
    doc.text('Expiración', 153.88, 51.61);
    const cadena = this.datos
    doc.setFontSize(21); 
    doc.text("#"+cadena, 40.53, 60.29);
    const productos = this.productos;
    //console.log(productos);
    const startX = 29.09; // Coordenada X
    const startY = 97.44; // Coordenada Y
    const data2 = [] as [number, string, number, number][];

    productos.forEach((data:any) => {
      const precioUnitario = data.PrecioUnitario.toFixed(2); // Formatear a 2 decimales
      data2.push([data.Cantidad, data.Descripcion, data.Unidad_Medida, precioUnitario ]);
    });

    //console.log(data2.length);
    
    const tableHeight = data2.length * 12; // Suponiendo 12 puntos de altura por fila
    const marginLeft = 28.56; // Ajusta el valor según el margen deseado

    autoTable(doc, {
      headStyles: { fillColor: [57, 57, 57] }, 
      head: [['EUA', 'Numero de parte', 'U/M', 'Precio Unitario']], 
      body: data2,
      startY: startY,
      tableWidth: 158, // Especificar el ancho de la tabla
      margin: {left: marginLeft }, // Especificar el margen izquierdo

      
    })
    const textY = tableHeight + 110.33; 
    doc.setFont("Helvetica", "normal");

    doc.setFontSize(10); 
    doc.text('El precio es en dólares americanos más IVA', 34.64, textY);
    doc.text('El precio es de acuerdo a la cantidad mínima', 34.64, textY+5.54);
    doc.text('marcada para cada material, si es menor', 34.64, textY+11.08);
    doc.text('cantidad el precio deberá de incrementarse.', 34.64, textY+16.64);

    doc.setFontSize(11); 

    doc.rect(137.39, textY, 22.17, 13.70, 'F');
    doc.setTextColor(255, 255, 255);
    doc.text('TOTAL', 142.46, textY+5.54);
    doc.rect(159.79, textY, 22.17, 13.70);
    doc.setTextColor(0,0,0);
    const precioTotalFormateado = parseFloat(this.datosCotizacion.Total).toFixed(2);
    doc.text('$'+precioTotalFormateado, 161.95, textY+5.54);

    // Dibujar una línea horizontal
    const xInicio = 28.56; // Coordenada x del punto de inicio
    const yInicio = textY+21.64; // Coordenada y del punto de inicio
    const xFin = 186.62;  // Coordenada x del punto de final
    const yFin = textY+21.64; ;   // Coordenada y del punto de final
    doc.setDrawColor(161, 161, 161); // Rojo en formato RGB
    doc.line(xInicio, yInicio, xFin, yFin);

    doc.rect(28.56, textY+33.12, 70.89, 52.49);

    doc.text('Contacto', 53.02, textY+36.64);
    doc.text('Observaciones', 132.31, textY+36.64);

    doc.setFontSize(9.5); 
    doc.setFont("Helvetica", "italic");
    doc.text('A partir de la segunda venta con OC abierta por parte', 103, textY+42.64);
    doc.text('del cliente, el tiempo de entrega es de 1 semana de acuerdo', 103, textY+47.64);
    doc.text('con los requerimientos del cliente, contando con', 103, textY+53.24);
    doc.text('un stock de seguridad de un mes en Parautos.', 103, textY+58.54);

    doc.text('Si la entrega es urgente, el precio aumenta debido a los', 103, textY+68.24);
    doc.text('costos de transportación.', 103, textY+73.54);

    doc.text('Capacitación mensual y seminario de la corrosión SIN costo', 103, textY+83.24);

    doc.text(this.datosUsuarioU.Nombre, 60, textY+47.64);
    doc.text(this.datosUsuarioU.ApellidoP, 60, textY+55.34);
    doc.text(this.datosUsuarioU.Rol, 60, textY+63.54);
    doc.text(this.datosUsuarioU.Telefono, 60, textY+71.24);
    doc.text(this.datosUsuarioU.email, 32.64, textY+78.94);
  
    //console.log(this.datosUsuarioU.imagen);
    const rutaImagen = this.datosUsuarioU.imagen; // Obtén esta ruta de tu base de datos
    this.urlImagen = `${URL2}${rutaImagen}`;
    //console.log(this.urlImagen);
    
    const vendedor =  this.urlImagen;
    doc.addImage(vendedor, 'PNG', 35, textY+44, 20, 26.55);
  
    //console.log("FechaEmision:", this.datosCotizacion.FechaEmision);
    //console.log("FechaExpiracion:", this.datosCotizacion.FechaExpiracion);
    
    doc.setFont("Helvetica", "bold");
    //const fechaParseada = new Date(this.datosCotizacion.FechaEmision);
    //const fechaE = format(fechaParseada, 'dd/MM/yyyy');
    //console.log("fecha", fechaE);
    doc.setFontSize(21); 
    doc.text( this.datosCotizacion.FechaEmision, 89.53, 60.29);

    
    //const fechaParseada2 = new Date(this.datosCotizacion.FechaExpiracion);
    //const fechaEX = format(fechaParseada2, 'dd/MM/yyyy');
    doc.text(this.datosCotizacion.FechaExpiracion, 145.30, 60.29);
    
    const datosC = this.datosCotizacion.Clientes_ID_Cliente.charAt(0).toUpperCase() + this.datosCotizacion.Clientes_ID_Cliente.slice(1).toLowerCase();
    doc.setFontSize(7); 
    const tableHeight2 = datosC.length * 7; // Suponiendo 12 puntos de altura por fila
    const marginLeft2 = 28.56; // Ajusta el valor según el margen deseado
    const data = [
      [datosC, this.datosCotizacion.Comentarios , datosC, this.datosCotizacion.Comentarios],
    ];
    autoTable(doc, {
      headStyles: { fillColor: [57, 57, 57] }, 
      head: [['Referencia', 'Vía de embarque', 'Lab', 'Terminos']], 
      body: data,
      startY: 71,
      tableWidth: 158, // Especificar el ancho de la tabla
      margin: {left: marginLeft2 }, // Especificar el margen izquierdo 
    })

    /*
    doc.setFillColor(backgroundColorN);
    doc.rect(29.09, 72.97, 160.02, 9.47, 'F');
    doc.setFontSize(12); 
    doc.setTextColor(255, 255, 255);
    doc.text("Referencia", 35.33, 78.66);
    doc.text("Vía de embarque", 74.12, 78.66);
    doc.text("Lab", 130.93, 78.66);
    doc.text("Terminos", 162.79, 78.66);

    */

    // Guarda el PDF
    doc.output('dataurlnewwindow')
    this.modalService.dismissAll()
    //doc.save('COTIZACION.pdf');
  }
}



 public urlImagen:any

 


  GenerarPDF2() {
    if(this.datosCotizacion.Status=='Revisar' || this.datosCotizacion.Status=='Rechazada'){
      alert("El estado de tu cotización debe ser Aprobada")
    }
    else if(this.datosCotizacion.Status=="Aprobada" ){
    const doc = new jsPDF({ unit: 'mm', format: [215.9, 279.4] }); // Custom size in millimeters (215.9 mm x 279.4 mm)
    doc.setFontSize(9);
    doc.rect(167.30, 2.76, 45.95, 21);
    doc.setFont("Helvetica", "bold");
    doc.text('Code: FO06-VEN-01', 173, 6.26);
    doc.text('Issue Date: 03/03/2023', 173, 10.16);
    doc.text('Revision: 01', 173, 14.94);
    doc.text('Page: 1 of 4', 173, 19.24);
    doc.rect(167.30, 2.76, 4, 21, 'F'); // 'F' indicates that the rectangle should be filled
  
    const imgData = "assets/img/ImgCoti/persona.jpg";
    // x, y, width, height
    doc.addImage(imgData, 'JPEG', 28.6, 60, 161, 172);
  
    doc.setFontSize(39.5);
    doc.text('BUSINESS PROPOSAL', 17.66, 43);
    doc.rect(0, 184.18, 215.9, 95, 'F');
  
    const imgData2 = "assets/img/ImgCoti/logo.png";
    doc.addImage(imgData2, 'PNG', 85.17, 214.20, 44.7, 41.7);
    doc.setFontSize(13);
    doc.setFont("Helvetica", "bold");
    doc.setTextColor(255, 255, 255);
    doc.text('Contact Us', 32.06, 234.7);
    doc.setFontSize(12);
    doc.text('support@parautos.com.mx', 16.08, 244.3);
    doc.text('7775006144', 16.08, 254.12);
    doc.text('www.parautos.com.mx', 16.08, 263.8);
  
    doc.setFontSize(13);
    doc.text('TAKE THE WORK OUT', 149.4, 251.6);
    doc.text('OF YOUR WORKDAY', 149.4, 259.05);
  
    doc.addPage();
    doc.rect(0, 0, 21.94, 279.4, 'F'); // 'F' indicates that the rectangle should be filled
    doc.setTextColor(0, 0, 0);
    doc.setFontSize(45.5);
    doc.text('About Us', 73.58, 24.32);
    doc.setFontSize(12);
    doc.text('For more than 20 years, we continue to pride ourselves on providing care', 40.38, 35.68);
    doc.text('and support to our customers, who enjoy EXCLUSIVE benefits such as:', 34.54, 42.95);
    doc.text('Digital Education, Free Support & Business Diagnostics.', 47.65, 49.18);
  
    doc.text('We have +7 carefully selected lines of business in order to provide your ', 40.05, 60);
    doc.text('company with exclusive benefits and solutions.', 53.05, 66.55);
    
    const anticorro = "assets/img/ImgCoti/anticorrosivos.jpg";
    doc.addImage(anticorro, 'JPEG', 46.76, 76.55, 61.65, 60.96);
    doc.setFontSize(20);
  
    const backgroundColor = '#FFFFFF';
    doc.setFillColor(backgroundColor);
    doc.rect(45.46, 104.12, 70.68, 9.16, 'F');
    doc.text('CORROSION SOLUTIONS', 35.76, 112);
  
    const lubri = "assets/img/ImgCoti/lubricantes.jpg";
    doc.addImage(lubri, 'JPEG', 116.69, 76.55, 61.65, 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(116.69, 104.12, 70.68, 9.16, 'F');
    doc.text('LUBRICANTS', 120.69, 112);
  
    const liq = "assets/img/ImgCoti/liquidoM.jpg";
    doc.addImage(liq, 'JPEG', 46.76, 145.32, 61.65, 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(46.76, 173.7, 70.68, 9.16, 'F');
    doc.text('MACHINED LIQUID', 47.69, 181.3);
  
    const hotmelt = "assets/img/ImgCoti/Hotmelts.jpg";
    doc.addImage(hotmelt, 'JPEG', 116.69, 145.32, 61.65, 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(116.69, 173.7, 70.68, 9.16, 'F');
    doc.text('HOTMELTS', 126.69, 181.3);
  
    const pla = "assets/img/ImgCoti/plastisol.jpg";
    doc.addImage(pla, 'JPEG', 46.76, 213.08, 61.65, 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(46.76, 244.75, 70.68, 9.16, 'F');
    doc.text('PLASTISOLS', 51.12, 252.75);
  
    const erp = "assets/img/ImgCoti/ERP.jpg";
    doc.addImage(erp, 'JPEG', 116.69, 213.08, 61.65, 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(116.69, 244.75, 70.68, 9.16, 'F');
    doc.text('ERP', 138.19, 252.75);
  
    doc.addPage();
    doc.setFontSize(45.5);
    doc.text('Quote', 28.16, 26.46);
    const backgroundColorN = '#393939';
    doc.setFillColor(backgroundColorN);
  
    const Logo2 = "assets/img/ImgCoti/parautosN.png";
    doc.addImage(Logo2, 'PNG', 184.11, 6, 27.77, 26.77);
    doc.rect(28.91, 46.75, 46.36, 23.44);
    doc.rect(28.91, 46.75, 3.98, 23.44, 'F');
  
    doc.rect(84.14, 46.75, 46.36, 23.44);
    doc.rect(84.14, 46.75, 3.98, 23.44, 'F');
    doc.rect(140.15, 46.75, 46.36, 23.44);
  doc.rect(140.15, 46.75, 3.98, 23.44, 'F');

  doc.setFontSize(10.5);
  doc.text('Quotation', 38.12, 51.61);
  doc.text('Date', 104.40, 51.61);
  doc.text('Expiration', 153.88, 51.61);
  const reference = this.datos
  doc.setFontSize(21);
  doc.text("#" + reference, 40.53, 60.29);
  const products = this.productos;
  //console.log(products);
  const startX = 29.09; // X coordinate
  const startY = 97.44; // Y coordinate
  const data2 = [] as [number, string, number, number][];

  products.forEach((data: any) => {
    //console.log(data);
    const unitPrice = data.PrecioUnitario.toFixed(2); // Format to 2 decimal places
    data2.push([data.Cantidad, data.Descripcion, data.Unidad_Medida, unitPrice]);
  });

  //console.log(data2.length);

  const tableHeight = data2.length * 12; // Assuming 12 points of height per row
  const marginLeft = 28.56; // Adjust the value as per the desired margin

  autoTable(doc, {
    headStyles: { fillColor: [57, 57, 57] },
    head: [['USA', 'Part Number', 'U/M', 'Unit Price']],
    body: data2,
    startY: startY,
    tableWidth: 158, // Specify the table width
    margin: { left: marginLeft },
  });

  const textY = tableHeight + 110.33;
  doc.setFont("Helvetica", "normal");

  doc.setFontSize(10);
  doc.text('The price is in US dollars + IVA', 34.64, textY);
  doc.text('The price is based on the minimum quantity', 34.64, textY + 5.54);
  doc.text('marked for each material, if it´s less', 34.64, textY + 11.08);
  doc.text('than the minimum quantity, the price should be increased.', 34.64, textY + 16.64);

  doc.setFontSize(11);

  doc.rect(137.39, textY, 22.17, 13.70, 'F');
  doc.setTextColor(255, 255, 255);
  doc.text('TOTAL', 142.46, textY + 5.54);
  doc.rect(159.79, textY, 22.17, 13.70);
  doc.setTextColor(0, 0, 0);
  const formattedTotalPrice =  parseFloat(this.datosCotizacion.Total).toFixed(2)
  doc.text('$' + formattedTotalPrice, 161.95, textY + 5.54);

  // Draw a horizontal line
  const xStart = 28.56; // x-coordinate of the starting point
  const yStart = textY + 21.64; // y-coordinate of the starting point
  const xEnd = 186.62; // x-coordinate of the ending point
  const yEnd = textY + 21.64; // y-coordinate of the ending point
  doc.setDrawColor(161, 161, 161); // Red in RGB format
  doc.line(xStart, yStart, xEnd, yEnd);

  doc.rect(28.56, textY + 33.12, 70.89, 52.49);

  doc.text('Contact', 53.02, textY + 36.64);
  doc.text('Remarks', 132.31, textY + 36.64);

  doc.setFontSize(9.5);
  doc.setFont("Helvetica", "italic");
  doc.text('From the second sale with an open PO by the customer,', 103, textY + 42.64);
  doc.text('the delivery time is 1 week according to ', 103, textY + 47.64);
  doc.text('customer requirements, with a one-month safety stock in Parautos.', 103, textY + 53.24);
  doc.text('If the delivery is urgent, the price increases due to transportation costs.', 103, textY + 58.54);

  doc.text('Monthly corrosion training and seminar free', 103, textY + 68.24);
    
  
  doc.text(this.datosUsuarioU.Nombre, 60, textY+47.64);
  doc.text(this.datosUsuarioU.ApellidoP, 60, textY+55.34);
  doc.text(this.datosUsuarioU.Rol, 60, textY+63.54);
  doc.text(this.datosUsuarioU.Telefono, 60, textY+71.24);
  doc.text(this.datosUsuarioU.email, 32.64, textY+78.94);

  //console.log(this.datosUsuarioU.imagen);
  const rutaImagen = this.datosUsuarioU.imagen; // Obtén esta ruta de tu base de datos
  this.urlImagen = `${URL2}${rutaImagen}`;
  //console.log(this.urlImagen);
  
  const vendedor =  this.urlImagen;
  doc.addImage(vendedor, 'PNG', 35, textY+44, 20, 26.55);


  doc.setFont("Helvetica", "bold");
  //const formattedDate = new Date(this.datosCotizacion.FechaEmision);
  //const dateStr = format(formattedDate, 'dd/MM/yyyy');
  //console.log("date", dateStr);
  doc.setFontSize(21);
  doc.text(this.datosCotizacion.FechaEmision, 89.53, 60.29);

  //const formattedExpirationDate = new Date(this.datosCotizacion.FechaExpiracion);
  //const expirationDateStr = format(formattedExpirationDate, 'dd/MM/yyyy');
  doc.text(this.datosCotizacion.FechaExpiracion, 145.30, 60.29);

  const datosC = this.datosCotizacion.Clientes_ID_Cliente.charAt(0).toUpperCase() + this.datosCotizacion.Clientes_ID_Cliente.slice(1).toLowerCase();
    doc.setFontSize(7); 
    const tableHeight2 = datosC.length * 7; // Suponiendo 12 puntos de altura por fila
    const marginLeft2 = 28.56; // Ajusta el valor según el margen deseado
    const data = [
      [datosC, this.datosCotizacion.Comentarios , datosC, this.datosCotizacion.Comentarios],
    ];
    autoTable(doc, {
      headStyles: { fillColor: [57, 57, 57] }, 
      head: [['Referencia', 'Vía de embarque', 'Lab', 'Terminos']], 
      body: data,
      startY: 71,
      tableWidth: 158, // Especificar el ancho de la tabla
      margin: {left: marginLeft2 }, // Especificar el margen izquierdo 
    })
    
  /*
  doc.setFillColor(backgroundColorN);
  doc.rect(29.09, 72.97, 160.02, 9.47, 'F');
  doc.setFontSize(12);
  doc.setTextColor(255, 255, 255);
  doc.text("Reference", 35.33, 78.66);
  doc.text("Shipping Method", 74.12, 78.66);
  doc.text("Lab", 130.93, 78.66);
  doc.text("Terms", 162.79, 78.66);
*/
  // Save the PDF
  doc.output('dataurlnewwindow')
  this.modalService.dismissAll()

}
}

  

  
}
