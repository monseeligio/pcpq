import { Component } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import * as moment from 'moment';
import { CotizacionService } from 'src/app/services/serviceUsurio/cotizacion.service';
import { Router } from '@angular/router';
import { HttpClient , HttpHeaders } from '@angular/common/http';



@Component({
  selector: 'app-cotizacion',
  templateUrl: './cotizacion.component.html',
  styleUrls: ['./cotizacion.component.css']
})
export class CotizacionComponent {
  datosCotizacion={FechaEmision:'', FechaExpiracion:'', Total:'', Clientes_ID_Cliente:'', Usuario_Clave_usuario:'', Comentarios:'', Status:''}

  constructor(private _service:UsuarioService, private _serviceC:CotizacionService, private _router: Router, private http: HttpClient){}

  public vendedores:any;
  public clientes:any;
  public fecha = moment().format("YYYY-MM-DD");
  public flete: string='Seleccione flete'
  public numC:any
  public productos:any;
  public ManualFlete:any
  valorIVA: number = 0.28; 
  ivaPorProducto: any = {};
  totalPrecioTotal: number = 0;
  preciosEnPesos: number[] = []; 
  public fleteToShow: number = 0; 
  fleteToShow2: number = 0; 
  public conversionRate: number = 1.0; // Tasa de conversión inicial
  public moneda:any
  public valorD:any

  ngOnInit(): void {
    this.getvendedores();
    this.getClientes();
    this.getProductosCotizados2()    
    this._serviceC.setNumeroCotizacionReal(this.numC);
    this.fleteToShow= parseInt(localStorage.getItem('Flete') || '');  
    this.obtener();
  }

  obtener(){
    //console.log("valor",this.ManualFlete)
    if (this.ManualFlete=='') {
      this.ManualFlete=0;
    }
    else{
      this.datosCotizacion.FechaExpiracion= localStorage.getItem('FechaExpiracion')|| '';
      this.datosCotizacion.Clientes_ID_Cliente=localStorage.getItem('Clientes_ID_Cliente') || '';
      this.datosCotizacion.Usuario_Clave_usuario= localStorage.getItem('Usuario_Clave_usuario') || '';
      this.datosCotizacion.Comentarios=localStorage.getItem('Comentarios') || '';
      this.ManualFlete= parseInt(localStorage.getItem('Flete') || '');  
      this.flete= localStorage.getItem('Flete2') || '';  
    }
  }

  actualizarFleteToShow() {
      if (this.flete === 'ingresar') {
          this.fleteToShow = this.ManualFlete; // Usar el valor manual ingresado
      } else {
          this.fleteToShow = parseInt(this.flete); // Usar el valor seleccionado de la lista
      }
  }

  /*
  actualizarTasaDeConversion(event: any) {
    this.moneda = event.target.value;
    const valor=this.valorD
    //console.log(valor);
    
    if (this.moneda === 'dolares') {
        //console.log("valor dolar: ",valor);
        this.conversionRate = 1 / valor // Cambiar MXN según la moneda destino deseada
      
    } else if (this.moneda === 'pesos') {
      this.conversionRate = 1.0; // Si ya ingresan los valores en pesos mexicanos
    }
  }
  */

actualizarTasaDeConversion() {
  if (this.moneda === 'dolares') {
    // Obtén el valor del dólar y conviértelo a número
    const valorDolar = parseFloat(this.valorD);

    // Verifica si el valor del dólar es un número válido
    if (valorDolar > 0) {
      this.conversionRate = 1 / valorDolar;
      //console.log("Tasa de conversión: ", this.conversionRate);
    } else {
      //console.log("Por favor, ingresa un valor válido para el dólar.");
      // Puedes agregar una lógica adicional aquí, como mostrar un mensaje de error al usuario.
    }
  } else if (this.moneda === 'pesos') {
    this.conversionRate = 1.0; // Si ya ingresan los valores en pesos mexicanos
  }
}


  calcularResultado(item: any, i: number, fleteToShow: number, moneda: string): number {
    const iva = (item.Precio * (this.ivaPorProducto[i] / 100 || 0.28 * 100)) * this.conversionRate; // Aplicar la conversión al precio antes de calcular el IVA
    const flete = (fleteToShow* this.conversionRate) / item.Cantidad;
    //console.log(this.conversionRate);
    
    //console.log(item.Precio*this.conversionRate);
    
    // Aplicar la conversión si la moneda es diferente de pesos mexicanos
    const precioUnitario = moneda === 'pesos' ? item.Precio + iva + flete : ((item.Precio*this.conversionRate) + iva + flete);
      
    const precioTotal = precioUnitario * item.Cantidad;
    const margen = 1 - ((item.Precio*this.conversionRate) / precioUnitario);
    const Desc = 0;
  
    item.VA = iva;
    item.Flete = flete;
    item.PrecioUnitario = precioUnitario;
    item.PrecioTotal = precioTotal;
    item.Desct = Desc;
    item.Margen = margen;
  
    this.totalPrecioTotal = this.productos.reduce(
      (total: number, producto: any) => total + producto.PrecioTotal,
      0
    );
  
    return margen;
  }

  
  getvendedores(){
    this._service.vendedores().subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.vendedores = resp;
        //console.log(resp);
    }
    })
  }

  getClientes(){
    this._service.clientes().subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.clientes = resp;
        //console.log(resp);
    }
    })
  }

  getProductosCotizados(){
    const numeroCotizacion = localStorage.getItem('numeroCotizacion');    
    this._service.obtenerProductosCotizados(numeroCotizacion).subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.productos = resp;
        //console.log(resp);
    }
    })
  }

  
  getProductosCotizados2(){
    this.productos = this._serviceC.obtenerProductos(); // Obtener la lista de productos
    const productosGuardados = localStorage.getItem('productos');
    if (productosGuardados) {
      this.productos = JSON.parse(productosGuardados);
    }
  }

  guardar(){
    if (this.datosCotizacion.FechaExpiracion=='' && this.datosCotizacion.Clientes_ID_Cliente=='' && this.datosCotizacion.Usuario_Clave_usuario=='' && this.datosCotizacion.Comentarios=='' && this.flete=='') {
      alert("Ingresa los datos para tu cotización")
    } else {
      localStorage.setItem('FechaEmision', this.fecha);
      localStorage.setItem('FechaExpiracion', this.datosCotizacion.FechaExpiracion);
      localStorage.setItem('Total', this.datosCotizacion.Total);
      localStorage.setItem('Clientes_ID_Cliente', this.datosCotizacion.Clientes_ID_Cliente);
      localStorage.setItem('Usuario_Clave_usuario', this.datosCotizacion.Usuario_Clave_usuario);
      localStorage.setItem('Comentarios', this.datosCotizacion.Comentarios);
      localStorage.setItem('Flete2', this.flete);
      localStorage.setItem('Flete', this.fleteToShow.toString());
      alert("Datos guardados temporalmente")
    }
  }

  getSumaPreciosTotales(): number {
    return this.totalPrecioTotal;
  }

  crearCotizacionYProductos() {
    const productos = this._serviceC.obtenerProductos(); // Obtener la lista de productos
    const datosCotizacionProductos = {
      FechaEmision: this.fecha,
      FechaExpiracion: this.datosCotizacion.FechaExpiracion,
      Clientes_ID_Cliente: this.datosCotizacion.Clientes_ID_Cliente,
      Usuario_Clave_usuario:  this.datosCotizacion.Usuario_Clave_usuario,
      Comentarios:  this.datosCotizacion.Comentarios,
      Productos: productos,
      Total:this.totalPrecioTotal
    };

    if (datosCotizacionProductos.Clientes_ID_Cliente=='') {
      alert("Ingresa los datos de tu cotizacion")
    } else {
      this._serviceC.crearCotizacionYProductos(datosCotizacionProductos).subscribe((data:any)=>{//LLAMA AL SERVICIO PARA AGREGAR AL USUARIO
    
        if (data.message == 'Datos insertados exitosamente') {
          alert("Registrado exitosamente");
            //console.log(datosCotizacionProductos);
        } else {
          alert("Error al registrar");
        }
        
      });
  
      localStorage.removeItem("FechaEmision");
      localStorage.removeItem("FechaExpiracion");
      localStorage.removeItem("Total");
      localStorage.removeItem("Clientes_ID_Cliente");
      localStorage.removeItem("Usuario_Clave_usuario");
      localStorage.removeItem("Comentarios");
      localStorage.removeItem("Flete");
      localStorage.removeItem("valorD");

      
    }
  }


  AgregarUser(){
    this.guardar()
       if(this.datosCotizacion.FechaExpiracion==''){
      window.alert("Debes ingresar todos los datos solicitados")
    }
    else{
      //console.log(this.datosCotizacion);//DATOS REGISTRADOS MOSTRADOS EN CONSOLA
      this._service.agregarCotizacion(this.datosCotizacion).subscribe((data:any)=>{//LLAMA AL SERVICIO PARA AGREGAR AL USUARIO
      this.numC=data.cotizacionId
      localStorage.setItem('numeroCotizacion', this.numC.toString());
      if (data.message == 'Datos insertados exitosamente') {
        alert("Registrado exitosamente");
          //console.log(this.datosCotizacion);
      } else {
        alert("Error al registrar");
      }
      
    })}
  }



  eliminarPC(id:any){
      this._service.eliminarProductoCotizado(id).subscribe((data:any)=>{
        ////console.log(id);
        this.getProductosCotizados();
      }) 
    }
  
  eliminarlocal(){
    localStorage.removeItem("FechaEmision");
    localStorage.removeItem("FechaExpiracion");
    localStorage.removeItem("Total");
    localStorage.removeItem("Clientes_ID_Cliente");
    localStorage.removeItem("Usuario_Clave_usuario");
    localStorage.removeItem("Comentarios");
    localStorage.removeItem("Flete");
    localStorage.removeItem("Flete2");
    localStorage.removeItem("productos");
    localStorage.removeItem("valorD");

    this._router.navigate(['menu']);

  }

  
  eliminarDatosCoti(){
    localStorage.removeItem("FechaEmision");
    localStorage.removeItem("FechaExpiracion");
    localStorage.removeItem("Total");
    localStorage.removeItem("Clientes_ID_Cliente");
    localStorage.removeItem("Usuario_Clave_usuario");
    localStorage.removeItem("Comentarios");
    localStorage.removeItem("Flete");
    localStorage.removeItem("Flete2");

    this._router.navigate(['cotizacion']);
  }
  
}
