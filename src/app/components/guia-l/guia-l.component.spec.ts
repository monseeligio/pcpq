import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuiaLComponent } from './guia-l.component';

describe('GuiaLComponent', () => {
  let component: GuiaLComponent;
  let fixture: ComponentFixture<GuiaLComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GuiaLComponent]
    });
    fixture = TestBed.createComponent(GuiaLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
