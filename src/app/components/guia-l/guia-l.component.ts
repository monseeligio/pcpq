import { Component } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import * as moment from 'moment';
import { CotizacionService } from 'src/app/services/serviceUsurio/cotizacion.service';
import { Router } from '@angular/router';
import { HttpClient , HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-guia-l',
  templateUrl: './guia-l.component.html',
  styleUrls: ['./guia-l.component.css']
})
export class GuiaLComponent {

  constructor(private _service:UsuarioService, private productosService:CotizacionService, private _router: Router){
  
  }
  ngOnInit(): void {
    this.obtenerValores()
  }
 
  public cantidad:any
  public productos:any;
  datosProductos={Productos_NumArticulo:'', Descripcion:'',Cotizacion_NumCotizacion:'', Cantidad:'', Unidad_Medida:'', Costo:'',Flete: '', Precio:'', VA:'', Desct:'', PrecioUnitario:'', PrecioTotal:'', Margen:''}


  opciones = ['DRY COAT RP', 'METAL RESCUE', 'ARMOR', 'RICHCUT', 'RICH CLEAN', 'VCI LIQUIDO', 'WEATHER',
      '55 GAL', '5 GAL', '19 LTS', '208 LTS'];

  public valoresD:any;

  opcionesDesecante: { [key: string]: boolean } = {};
  
  obtenerValores() {
    this.valoresD = [];
    for (const opcion in this.opcionesDesecante) {
      if (this.opcionesDesecante[opcion]) {
        this.valoresD.push(opcion);
      }
    }

    //console.log(this.valoresD);
  }

  getProductos(){
    this.valoresD = [];
    for (const opcion in this.opcionesDesecante) {
      if (this.opcionesDesecante[opcion]) {
        this.valoresD.push(opcion);
      }
    }
    //console.log(this.valoresD);
  
    this._service.ventaguiadaL(this.valoresD).subscribe((resp:any)=>{
      if (Array.isArray(resp)) { 
        this.productos = resp;
        //console.log(resp);
    }
    })
  }
  
  getImageUrl(imageId: any) {
    //console.log("mostrar id",imageId);
    return this._service.getImagen(imageId)
  }

  imagenError(event: any) {
    //console.log('Error al cargar la imagen:', event);
  }

  agregarProducto(producto:any) {
    this.datosProductos.Productos_NumArticulo=producto.NumArticulo
    this.datosProductos.Productos_NumArticulo=producto.NumArticulo
    this.datosProductos.Descripcion=producto.Descripcion
    this.datosProductos.Cantidad=producto.Cantidad
    this.datosProductos.Unidad_Medida=producto.Unidad_Medida
    //console.log(producto);
    this.datosProductos.Costo=producto.Costo
    if(producto.Cantidad==null || producto.Cantidad=='0'){
      alert("Ingrese la cantidad de producto")
    }
    else{
    this.productosService.agregarProducto(this.datosProductos);
    this.datosProductos={Productos_NumArticulo:'', Descripcion:'',Cotizacion_NumCotizacion:'', Cantidad:'', Unidad_Medida:'', Costo:'' ,Flete: '', Precio:'', VA:'', Desct:'', PrecioUnitario:'', PrecioTotal:'', Margen:''}
  
    this._router.navigate(['cotizacion']);
    }
  }

  validarInput(event: any) {
    const input = event.target;
    input.value = input.value.replace(/[^0-9]/g, ''); // Elimina caracteres no numéricos
  }


}

