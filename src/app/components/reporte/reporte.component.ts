import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import { CotizacionService } from 'src/app/services/serviceUsurio/cotizacion.service';
import { Router } from '@angular/router';
import Chart from 'chart.js/auto';
import { mergeScan } from 'rxjs';

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.css']
})

export class ReporteComponent implements OnInit {
  constructor(private _serviceM:UsuarioService, private productosService:CotizacionService, private _router: Router){}
    datos:any;
    productos:any
    datosPorMes:any;
    public seleccione:string='Seleccione'
    years: number[] = [2023, 2024,2025,2026]; 
    mesesLabels = [
      'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio',
      'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'
    ];
    selectedYear:number = new Date().getFullYear();
    
    productosMasVendidos:any
    CotizacionesVendores:any

    CotizacionesVendoresStatus:any

    categorias:any

    selectedMonth:number=new Date().getMonth()+1;
    selectedanio:number=new Date().getFullYear();

    selectedMonth2:number=new Date().getMonth()+1;
    selectedanio2:number=new Date().getFullYear();

    ngOnInit(): void {
      this._serviceM.obtenerCotizaciones().subscribe((resp:any)=>{
        if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
          this.datos = resp;          
      }
      this.obtener()
      })

      this._serviceM.obtenerProductosCotizacidos().subscribe((resp:any)=>{
        if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
          this.productos = resp;        
          //console.log(this.productos);
            
      }
      this.obtener2();
      this.obtener3();
      }) 
    } 


    obtener(){
      this.datosPorMes = this.cotizacionesAnio(this.datos, this.selectedYear);      
      this.Grafico1Anios()
    }

    obtener2(): void {
      const mes: number  = this.selectedMonth;
      const anio:number  = this.selectedanio; // Aquí define el año que desees
      //console.log(mes,anio);
      this.productosMasVendidos = this.obtenerProductosMasVendidos(this.datos, mes, anio)
      //console.log(this.productosMasVendidos);
      this.generarGrafico2();
      this.categorias = this.obtenerProductosMasVendidosPorCategoria(this.datos, mes, anio);
      console.log(this.categorias);
      this.generarGrafico3();
    }

    obtener3(): void {
      const mes: number  = this.selectedMonth2;
      const anio:number  = this.selectedanio2; // Aquí define el año que desees
      //console.log(mes,anio);
      this.CotizacionesVendores = this.obtenerCotizacionesPorVendedor(this.datos, mes, anio)
      console.log("datos vendedores",this.CotizacionesVendores);
      this.crearGraficoBarras()
      this.CotizacionesVendoresStatus = this.obtenerCotizacionesPorEstado(this.datos, mes, anio)
      console.log(this.CotizacionesVendoresStatus);
      this.crearYMostrarGraficoPorEstado();

      
    }


    cotizacionesAnio(datos: any[], añoSeleccionado: number): any[] {
      //console.log(datos, añoSeleccionado, "datos");
      const datosFiltrados = datos.filter(cotizacion => {
        const fecha = new Date(cotizacion.FechaEmision);
        return fecha.getFullYear() == añoSeleccionado;
      });
    //console.log(datosFiltrados);    
      const ventasPorMes: { [clave: string]: number } = {};
      datosFiltrados.forEach(cotizacion => {
        const fecha = new Date(cotizacion.FechaEmision);
        const mes = fecha.getMonth() + 1;
        const clave = `${fecha.getFullYear()}-${mes}`;
        if (!ventasPorMes[clave]) {
          ventasPorMes[clave] = 0;
        }
        ventasPorMes[clave] += cotizacion.Total;
      });
      /*
      const datosPorMes = Object.keys(ventasPorMes).map(clave => ({
        mes: clave,
        ventas: ventasPorMes[clave]
      }));*/
      const datosPorMes = Object.keys(ventasPorMes).map(clave => {
        const [anio, mes] = clave.split('-');
        const nombreMes = this.mesesLabels[parseInt(mes) - 1] || ''; 
        return {
          mes: `${nombreMes} ${anio}`,
          ventas: ventasPorMes[clave]
        };
      });
      return datosPorMes;
    }
    myChart: Chart | undefined;
    Grafico1Anios(): void {
      const meses = this.datosPorMes.map((dato: { mes: string }) => dato.mes);
      const ventas = this.datosPorMes.map((dato: { ventas: number }) => dato.ventas);
  
      const ctx = document.getElementById('myChart') as HTMLCanvasElement;
  
      if (!this.myChart) {
        // Si el gráfico no existe, crear uno nuevo
        this.myChart = new Chart(ctx, {
          type: 'line',
          data: {
            labels: meses,
            datasets: [{
              label: 'Cotizaciones por mes',
              data: ventas,
              backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
              ],
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              y: {
                beginAtZero: true
              }
            }
          }
        });
      } else {
        // Si el gráfico ya existe, actualizar los datos
        this.myChart.data.labels = meses;
        this.myChart.data.datasets[0].data = ventas;
        this.myChart.update();
      }
    }

    obtenerProductosMasVendidos(datos: any[], mes: number, anio: number): { Producto_NumArticulo: string, CantidadVendida: number }[] {
      const cotizacionesMesAnio = datos.filter(cotizacion => {
          const fechaCotizacion = new Date(cotizacion.FechaEmision);
          //console.log("Fecha de cotización:", fechaCotizacion); // Agregado para verificar la fecha
          //console.log("Mes y año:", mes, anio);
          return fechaCotizacion.getMonth()+1 == mes && fechaCotizacion.getFullYear()== anio;
      });
      //console.log(cotizacionesMesAnio);

      const numerosCotizacionMesAnio = cotizacionesMesAnio.map(cotizacion => cotizacion.NumCotizacion);
      const ventasMesAnio = this.productos.filter((venta:any) => numerosCotizacionMesAnio.includes(venta.Cotizacion_NumCotizacion));
      // Calcular el total de ventas de cada producto
      const ventasPorProducto: { [key: string]: number } = {};
      ventasMesAnio.forEach((venta:any) => {
        const productoNumArticulo = venta.Productos_NumArticulo;
        if (productoNumArticulo in ventasPorProducto) {
          ventasPorProducto[productoNumArticulo] += venta.Cantidad;        
        } else {
          ventasPorProducto[productoNumArticulo] = venta.Cantidad;
        }
      //console.log(numerosCotizacionMesAnio);

      });
      // Convertir el objeto ventasPorProducto a un array de objetos
      const productosMasVendidos = Object.keys(ventasPorProducto).map(key => ({
        Producto_NumArticulo: key,
        CantidadVendida: ventasPorProducto[key]
      }));
      // Ordenar los productos por cantidad vendida
      productosMasVendidos.sort((a, b) => b.CantidadVendida - a.CantidadVendida);
      // Seleccionar los 20 productos más vendidos    
      return productosMasVendidos.slice(0, 20);
    }
    myChart2: Chart | undefined;
    generarGrafico2(): void {
      const ctx = document.getElementById('productos') as HTMLCanvasElement;
      const productos = this.productosMasVendidos.map((producto:any )=> producto.Producto_NumArticulo);
      const cantidades = this.productosMasVendidos.map((producto:any )=> producto.CantidadVendida);
      // Definir una paleta de colores fuertes para 20 barras
      const colores = [
        'rgba(255, 0, 0, 0.2)',    // Rojo
        'rgba(0, 255, 0, 0.2)',    // Verde
        'rgba(0, 0, 255, 0.2)',    // Azul
        'rgba(255, 255, 0, 0.2)',  // Amarillo
        'rgba(255, 0, 255, 0.2)',  // Magenta
        'rgba(0, 255, 255, 0.2)',  // Cian
        'rgba(255, 165, 0, 0.2)',  // Naranja
        'rgba(128, 0, 128, 0.2)',  // Púrpura
        'rgba(255, 69, 0, 0.2)',   // Naranja rojizo
        'rgba(0, 128, 0, 0.2)',    // Verde oscuro
        'rgba(0, 0, 128, 0.2)',    // Azul oscuro
        'rgba(128, 0, 0, 0.2)',    // Rojo oscuro
        'rgba(128, 128, 0, 0.2)',  // Verde oliva
        'rgba(0, 128, 128, 0.2)',  // Azul verdoso
        'rgba(128, 0, 128, 0.2)',  // Morado
        'rgba(0, 255, 128, 0.2)',  // Verde azulado
        'rgba(255, 128, 0, 0.2)',  // Naranja claro
        'rgba(0, 255, 255, 0.2)',  // Cian claro
        'rgba(255, 0, 128, 0.2)',  // Rosa
        'rgba(128, 128, 128, 0.2)' // Gris
      ];
    
      if (!this.myChart2) {
        this.myChart2 = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: productos,
            datasets: [{
              label: 'Productos más vendidos',
              data: cantidades,
              backgroundColor: colores, // Usar colores fuertes
              borderColor: colores,
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              y: {
                beginAtZero: true
              }
            }
          }
        });
      } else {
        // Si el gráfico ya existe, actualizar los datos
        this.myChart2.data.labels = productos;
        this.myChart2.data.datasets[0].data = cantidades;
        // También necesitas actualizar los colores si los datos cambian
        this.myChart2.data.datasets[0].backgroundColor = colores;
        this.myChart2.update();
      } 
    }
    
    obtenerProductosMasVendidosPorCategoria = (datos: any[], mes: number, anio: number) => {
      const categorias = ['DES', 'HOT', 'LIQ', 'PLA', 'VCI'];
      const ventasPorCategoria: { [categoria: string]: number } = {};

      // Inicializar recuento de ventas por categoría
      categorias.forEach(categoria => ventasPorCategoria[categoria] = 0);

      // Filtrar las cotizaciones por el mes y año seleccionados
      const cotizacionesMesAnio = datos.filter(cotizacion => {
          const fechaCotizacion = new Date(cotizacion.FechaEmision);
          return fechaCotizacion.getMonth() === mes - 1 && fechaCotizacion.getFullYear() == anio;
      });

      // Iterar sobre las cotizaciones filtradas y sumar las ventas por categoría
      cotizacionesMesAnio.forEach(cotizacion => {
          const numeroCotizacion = cotizacion.NumCotizacion;
          const productosCotizacion = this.productos.filter((producto: any) => producto.Cotizacion_NumCotizacion === numeroCotizacion);
          productosCotizacion.forEach((producto:any) => {
              categorias.forEach(categoria => {
                  if (producto.Productos_NumArticulo.includes(categoria)) {
                      ventasPorCategoria[categoria] += producto.Cantidad;
                  }
              });
          });
      });

      return ventasPorCategoria;
    };
    myChart3: Chart | undefined;
    generarGrafico3(): void {
      const ctx = document.getElementById('categorias') as HTMLCanvasElement;
      const categorias = ['DES', 'HOT', 'LIQ', 'PLA', 'VCI'];
    
      // Definir una paleta de colores fuertes para 5 barras
      const colores = [
        'rgba(255, 0, 0, 0.2)',    // Rojo
        'rgba(0, 255, 0, 0.2)',    // Verde
        'rgba(0, 0, 255, 0.2)',    // Azul
        'rgba(255, 255, 0, 0.2)',  // Amarillo
        'rgba(255, 0, 255, 0.2)'   // Magenta
      ];
    
      if (!this.myChart3) {
        this.myChart3 = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: categorias,
            datasets: [{
              label: 'Categorías más vendidas',
              data: this.categorias,
              backgroundColor: colores, // Usar colores fuertes
              borderColor: colores,
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              y: {
                beginAtZero: true
              }
            }
          }
        });
      } else {
        // Si el gráfico ya existe, actualizar los datos
        this.myChart3.data.labels = categorias;
        this.myChart3.data.datasets[0].data = this.categorias;
        // También necesitas actualizar los colores si los datos cambian
        this.myChart3.data.datasets[0].backgroundColor = colores;
        this.myChart3.update();
      }
    }



    obtenerCotizacionesPorVendedor(datos: any[], mes: number, anio: number): Record<string, number> {
      // Filtra las cotizaciones para obtener solo las del mes y año seleccionados
      const cotizacionesFiltradas = datos.filter((cotizacion:any) => {
        const fechaEmision = new Date(cotizacion.FechaEmision);
        return fechaEmision.getMonth() + 1 == mes && fechaEmision.getFullYear() == anio;
      });
      const cotizacionesPorVendedor: Record<string, number> = {};
      // Procesa las cotizaciones filtradas para contar cuántas ha realizado cada vendedor
      for (const cotizacion of cotizacionesFiltradas) {        
        const vendedor = cotizacion.Nombre;
        cotizacionesPorVendedor[vendedor] = (cotizacionesPorVendedor[vendedor] || 0) + 1;
      }
      
      // Retorna el objeto con las cotizaciones por vendedor
      return cotizacionesPorVendedor;
    }
    myChart4: Chart<"bar", unknown[], string> | undefined;
    crearGraficoBarras(): void {
    const nombresVendedores = Object.keys(this.CotizacionesVendores);
    const cantidadesCotizaciones = Object.values(this.CotizacionesVendores);
    const colores = [
      'rgba(255, 0, 0, 0.2)',    // Rojo
      'rgba(0, 255, 0, 0.2)',    // Verde
      'rgba(0, 0, 255, 0.2)',    // Azul
      'rgba(255, 255, 0, 0.2)',  // Amarillo
      'rgba(255, 0, 255, 0.2)'   // Magenta
    ];
    const ctx = document.getElementById('vendedores') as HTMLCanvasElement;

    if (!this.myChart4) {
      // Si myChart4 no está definido, crea un nuevo gráfico de barras
      this.myChart4 = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: nombresVendedores,
          datasets: [{
            label: 'Cotizaciones por Vendedor',
            data: cantidadesCotizaciones,
            backgroundColor: colores,
            borderColor: colores,
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true,
            }
          }
        }
      });
    } else {
      // Si myChart4 ya está definido, actualiza los datos del gráfico
      this.myChart4.data.labels = nombresVendedores;
      this.myChart4.data.datasets[0].data = cantidadesCotizaciones;
      this.myChart4.data.datasets[0].backgroundColor = colores;
      this.myChart4.update();
    }
    }
    obtenerCotizacionesPorEstado(datos: any[], mes: number, anio: number): { aprobadas: number, rechazadas: number, enRevision: number } {
      // Filtra las cotizaciones para obtener solo las del mes y año seleccionados
      const cotizacionesFiltradas = this.datos.filter((cotizacion:any )=> {
        const fechaEmision = new Date(cotizacion.FechaEmision);
        return fechaEmision.getMonth() + 1 == mes && fechaEmision.getFullYear() == anio;
      });

      // Inicializa el objeto para contabilizar el recuento de cotizaciones por estado
      const cotizacionesPorEstado = { aprobadas: 0, rechazadas: 0, enRevision: 0 };

      // Procesa las cotizaciones filtradas para contar cuántas están en cada estado
      for (const cotizacion of cotizacionesFiltradas) {
        switch (cotizacion.Status) {
          case 'Aprobada':
            cotizacionesPorEstado.aprobadas++;
            break;
          case 'Rechazada':
            cotizacionesPorEstado.rechazadas++;
            break;
          case 'Revisar':
            cotizacionesPorEstado.enRevision++;
            break;
          default:
            // Si el estado no coincide con ninguno de los casos anteriores, no se hace nada
            break;
        }
      }

      // Retorna el objeto con el recuento de cotizaciones por estado
      return cotizacionesPorEstado;
    }

    myChart5: Chart<"bar", unknown[], string> | undefined;
    crearYMostrarGraficoPorEstado(): void {
      const labels = ['Aprobadas', 'Rechazadas', 'En revisión'];
      const colores = [
        'rgba(255, 0, 0, 0.2)',    // Rojo
        'rgba(0, 255, 0, 0.2)',    // Verde
        'rgba(0, 0, 255, 0.2)',    // Azul
        'rgba(255, 255, 0, 0.2)',  // Amarillo
        'rgba(255, 0, 255, 0.2)'   // Magenta
      ];
      const data = [
        this.CotizacionesVendoresStatus.aprobadas,
        this.CotizacionesVendoresStatus.rechazadas,
        this.CotizacionesVendoresStatus.enRevision
      ];
    
      const ctx = document.getElementById('aprobaciones') as HTMLCanvasElement;
      if (!this.myChart5) {
        this.myChart5 = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: labels,
            datasets: [{
              label: 'Cotizaciones por Estado',
              data: data,
              backgroundColor: colores,
              borderColor: colores,
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              y: {
                beginAtZero: true,
              }
            }
          }
        });
      } else {
        // Si el gráfico ya existe, actualizar los datos
        this.myChart5.data.labels = labels;
        this.myChart5.data.datasets[0].data = data;
        // También necesitas actualizar los colores si los datos cambian
        this.myChart5.data.datasets[0].backgroundColor = colores;
        this.myChart5.update();
      }
    }

    
}
  
