import { Component } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import { CotizacionService } from 'src/app/services/serviceUsurio/cotizacion.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-aprobaciones',
  templateUrl: './aprobaciones.component.html',
  styleUrls: ['./aprobaciones.component.css']
})


export class AprobacionesComponent {
  datos:any;
  usuario:any
  notificaciones:any
  constructor(private _serviceM:UsuarioService, private productosService:CotizacionService, private _router: Router){}
  public datosCotizacionAprobar:any
  
  ngOnInit(): void {
    this.getRol()
    this.usuario = localStorage.getItem('clave');

  }

  getRol(){
    this.usuario = localStorage.getItem('clave');
    //console.log("USUARIO", this.usuario);
    this._serviceM.obtenerCotizacionesRevisar(this.usuario).subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.datos = resp;
        //console.log(this.datos.length);
        
    }
    })
  }

  obtenerID(item:any){
    this.productosService.enviarID(item)    
    this._router.navigate(['aprobar-cotizacion']);
  }



}