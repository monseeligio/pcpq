import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuiaPComponent } from './guia-p.component';

describe('GuiaPComponent', () => {
  let component: GuiaPComponent;
  let fixture: ComponentFixture<GuiaPComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GuiaPComponent]
    });
    fixture = TestBed.createComponent(GuiaPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
