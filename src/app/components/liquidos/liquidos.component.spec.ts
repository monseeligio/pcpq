import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiquidosComponent } from './liquidos.component';

describe('LiquidosComponent', () => {
  let component: LiquidosComponent;
  let fixture: ComponentFixture<LiquidosComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LiquidosComponent]
    });
    fixture = TestBed.createComponent(LiquidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
