import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizacionusuarioComponent } from './cotizacionusuario.component';

describe('CotizacionusuarioComponent', () => {
  let component: CotizacionusuarioComponent;
  let fixture: ComponentFixture<CotizacionusuarioComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CotizacionusuarioComponent]
    });
    fixture = TestBed.createComponent(CotizacionusuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
