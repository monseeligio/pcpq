import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlastisolComponent } from './plastisol.component';

describe('PlastisolComponent', () => {
  let component: PlastisolComponent;
  let fixture: ComponentFixture<PlastisolComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlastisolComponent]
    });
    fixture = TestBed.createComponent(PlastisolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
