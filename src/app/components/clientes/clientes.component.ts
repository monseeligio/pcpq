import { Component } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';


@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent {
  constructor(private _serviceM:UsuarioService){}

  datosUsuario={ID_cliente:'', Razon_Social:'', RFC:'', Domicilio_Fiscal:'', email:'', telefono_empresarial:''}
  ngOnInit(): void {
  }

  AgregarCliente() {
    if (
      this.datosUsuario.ID_cliente == '' ||
      this.datosUsuario.Razon_Social == '' 
      ) {
      window.alert('Debes ingresar todos los datos solicitados');

    } else if (this.datosUsuario.RFC && !this.validarRFC(this.datosUsuario.RFC)) {
      window.alert('RFC inválido. Por favor, ingresa un RFC válido.');
      }
    else if (this.datosUsuario.email && !this.validarCorreo(this.datosUsuario.email)) {
      window.alert('Correo no inválido. Por favor, ingresa un correo válido.');
    }
    else {
      //console.log(this.datosUsuario);
      this._serviceM.agregarCliente(this.datosUsuario).subscribe((data: any) => {
        if (data.message == 'Datos insertados exitosamente') {
          alert('Registrado exitosamente');
        } else if (data.message == 'El usuario con este ID_cliente ya existe') {
          alert('El ID del usuario ya se encuentra registrado');
        } else if (data.message == 'El usuario con este RFC ya existe') {
          alert('El usuario con este RFC ya existe');
        } else {
          alert('Error al registrar el usuario');
        }
      });
    }
  }
  
  validarRFC(rfc: string): boolean | undefined {
    // Verificar si rfc tiene un valor definido y no es null ni undefined
    if (rfc != null && typeof rfc === 'string') {
      // Expresión regular para validar RFC
      const rfcRegex = /^[A-Z&Ñ]{3,4}\d{6}[A-Z\d]{3}$/;
      return rfc.match(rfcRegex) !== null;
    }
    // No hacer nada si rfc no se ha proporcionado
    return undefined;
  }
  
  
  validarCorreo(email:any) {
    const regexCorreo = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
    return regexCorreo.test(email);
  }


}
