import { Component } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import { CotizacionService } from 'src/app/services/serviceUsurio/cotizacion.service';
import { Router } from '@angular/router';
import Chart from 'chart.js/auto';



@Component({
  selector: 'app-cotizaciones',
  templateUrl: './cotizaciones.component.html',
  styleUrls: ['./cotizaciones.component.css']
})
export class CotizacionesComponent {
  datos:any;
  datosPorMes:any;

  public vendedores:any
  public usuario:string='Representante de ventas'
  constructor(private _serviceM:UsuarioService, private productosService:CotizacionService, private _router: Router){}
  public seleccione:string='Seleccione'
  ngOnInit(): void {
    this.getRol()
    this.getvendedores()
  }

  getRol(){
    this._serviceM.obtenerCotizaciones().subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.datos = resp;
        //this.datos.reverse();
        
        this.datosPorMes = this.procesarDatos(this.datos);
        this.crearGrafico();
    }
    })
    
  }
  getColorClass(status: string) {
    switch (status) {
      case 'Revisar':
        return 'status-amarillo';
      case 'Aprobada':
        return 'status-verde';
      case 'Rechazada':
        return 'status-rojo';
      default:
        return ''; // Si el estado no coincide con ninguno de los casos anteriores, no se aplicará ningún estilo.
    }
  }
  

  obtenerID(item:any){
    this.productosService.enviarID(item)    
    this._router.navigate(['visualizar-cotizacion']);
  }

  getvendedores(){
    this._serviceM.vendedores().subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.vendedores = resp;
        //console.log(resp);
    }
    })
  }

  getCotiPorusuario(){
    this._serviceM.getCotiUsuario(this.usuario).subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.datos = resp;
    }
    })
  }


  procesarDatos(datos: any[]): any[] {
    const ventasPorMes: { [clave: string]: number } = {}; // Definir el tipo de ventasPorMes
    console.log(ventasPorMes);
    datos.forEach(cotizacion => {
      const fecha = new Date(cotizacion.FechaEmision);
      const mes = fecha.getMonth() + 1; // Los meses en JavaScript van de 0 a 11, por eso sumamos 1
      const anio = fecha.getFullYear();
      const clave = `${anio}-${mes}`;
      if (!ventasPorMes[clave]) {
        ventasPorMes[clave] = 0;
      }
      ventasPorMes[clave] += cotizacion.Total;
    });
  
    // Convertir el objeto a un arreglo de objetos para facilitar su manejo en la interfaz de usuario
    const datosPorMes = Object.keys(ventasPorMes).map(clave => ({
      mes: clave,
      ventas: ventasPorMes[clave]
    }));    
    return datosPorMes;
  }
  

  crearGrafico(): void {
    const meses = this.datosPorMes.map((dato: { mes: string }) => dato.mes);
    const ventas = this.datosPorMes.map((dato: { ventas: number }) => dato.ventas);

    const ctx = document.getElementById('myChart') as HTMLCanvasElement;
    const myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: meses,
        datasets: [{
          label: 'Ventas por mes',
          data: ventas,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  }
  


  limpiar(){
    this.usuario='Representante de ventas';
    this.getRol()
  }

}
