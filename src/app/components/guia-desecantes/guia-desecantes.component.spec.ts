import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuiaDesecantesComponent } from './guia-desecantes.component';

describe('GuiaDesecantesComponent', () => {
  let component: GuiaDesecantesComponent;
  let fixture: ComponentFixture<GuiaDesecantesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GuiaDesecantesComponent]
    });
    fixture = TestBed.createComponent(GuiaDesecantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
