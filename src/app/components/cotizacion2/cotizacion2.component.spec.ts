import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Cotizacion2Component } from './cotizacion2.component';

describe('Cotizacion2Component', () => {
  let component: Cotizacion2Component;
  let fixture: ComponentFixture<Cotizacion2Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [Cotizacion2Component]
    });
    fixture = TestBed.createComponent(Cotizacion2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
