import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import * as moment from 'moment';
import { CotizacionService } from 'src/app/services/serviceUsurio/cotizacion.service';
import { Router } from '@angular/router';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';


import jsPDF from 'jspdf';
import { format } from 'date-fns';
import autoTable from 'jspdf-autotable'


import { URL, URL2 } from '../../config/config';




@Component({
  selector: 'app-cotizacion2',
  templateUrl: './cotizacion2.component.html',
  styleUrls: ['./cotizacion2.component.css']
})

export class Cotizacion2Component {

  @ViewChild("update", {static: false}) myModalInfo!: TemplateRef<any>; 
  @ViewChild("pdf", {static: false}) myModalPDF!: TemplateRef<any>; 
  
  datosCotizacion={FechaEmision:'', FechaExpiracion:'', Total:'', Clientes_ID_Cliente:'Seleccione', Usuario_Clave_usuario:'Seleccione', Comentarios:'', Status:''}

  constructor(private _service:UsuarioService, private _serviceC:CotizacionService, private _router: Router, private http: HttpClient, private modalService: NgbModal){
    const today = new Date();
    this.minDate = today.toISOString().split('T')[0]; // Formato YYYY-MM-DD
   

  }
  
  datosUsuarioU={Clave_usuario:'', Nombre:'', ApellidoP:'', ApellidoM:'', Rol:'', email:'', Status:'', Password:''}

 public fecha30DiasDespues:any
  public vendedores:any;
  public gerentes:any;
  public clientes:any;
  public fecha = moment().format("YYYY-MM-DD");
  public fechaE = moment(this.fecha).add(30, 'days').format("YYYY-MM-DD");
  public flete: string='Seleccione flete'
  public numC:any
  public productos:any;
  public ManualFlete:any
  valorIVA: number = 0.28; 
  ivaPorProducto: any = {};
  totalPrecioTotal: number = 0;
  preciosEnPesos: number[] = []; 
  public fleteToShow: number = 0; 
  fleteToShow2: number = 0; 
  public conversionRate: number = 1.0; 
  public moneda:any
  public DescT: number = 0
 public margen:any; 
 notificaciones: any[] = [];
 public selectedDate:any; 
 public minDate: any; 
 public idioma:any


  ngOnInit(): void {
    this.getvendedores();
    this.getClientes();
    this.getGerentes()
    this.getProductosCotizados2()    
    this._serviceC.setNumeroCotizacionReal(this.numC);
    this.fleteToShow= parseInt(localStorage.getItem('Flete') || '');  
    this.obtener();
  }


  mostrarModalInfo(){
    this.modalService.open(this.myModalInfo);
  }

  mostrarModalPDF(){
    this.modalService.open(this.myModalPDF);
  }


  eliminar(index: number) {
    if (index >= 0 && index < this.productos.length) {
      this.productos.splice(index, 1);
      //alert("Producto eliminado");
    }
  }
  
  obtener(){
    //console.log("valor",this.ManualFlete)
    if (this.ManualFlete=='') {
      this.ManualFlete=0;
    }
    else{
      this.datosCotizacion.FechaExpiracion= localStorage.getItem('FechaExpiracion')|| '';
      this.datosCotizacion.Clientes_ID_Cliente=localStorage.getItem('Clientes_ID_Cliente') || '';
      this.datosCotizacion.Usuario_Clave_usuario= localStorage.getItem('Usuario_Clave_usuario') || '';
      this.datosCotizacion.Comentarios=localStorage.getItem('Comentarios') || '';
      this.flete= localStorage.getItem('Flete2') || '';  
      this.moneda= localStorage.getItem('Moneda') || '';
      const flete1=parseInt(localStorage.getItem('fleteToShow') || '');
      //console.log(flete1);
        const flete= parseInt(localStorage.getItem('Flete') || '');
        if(Number.isNaN(flete)){
          this.ManualFlete=0;
        }
        else{
          this.ManualFlete=flete;
        }

      
    }
  }

  actualizarFleteToShow() {
      if (this.flete === 'ingresar') {
          this.fleteToShow = this.ManualFlete; // Usar el valor manual ingresado
      } else {
          this.fleteToShow = parseInt(this.flete); // Usar el valor seleccionado de la lista
      }
  }


  getExchangeRate() {
    const accessKey = 'ab45071db7310de683031f0b'; // Reemplaza con tu clave de acceso API
    const exchangeRatesApiUrl = `https://v6.exchangerate-api.com/v6/${accessKey}/latest/USD`;
    return this.http.get(exchangeRatesApiUrl);
  }

  actualizarTasaDeConversion(event: any) {
    this.moneda = event.target.value; // Obtener el valor seleccionado del select
    if (this.moneda === 'dolares') {
      this.getExchangeRate().subscribe((data: any) => {
        //console.log(data.conversion_rates['MXN']);
        this.conversionRate = 1 / data.conversion_rates['MXN']; // Cambiar MXN según la moneda destino deseada
      });
    } else if (this.moneda === 'pesos') {
      this.conversionRate = 1.0; // Si ya ingresan los valores en pesos mexicanos
    }
  }
  




  mostrarSolicitarAprobacion: boolean = false;
  calcularResultado(item: any, i: number, fleteToShow: number, moneda: string): number {
    //console.log(fleteToShow);
    if(item.Desct==''){
      item.Desct=0;
    }
    if(this.ivaPorProducto[i]==''){
      item.VA=0;
    }
    if(fleteToShow==3){
      const iva = (item.Costo * (this.ivaPorProducto[i] / 100 || 0.29 )) * this.conversionRate; // Aplicar la conversión al precio antes de calcular el IVA
      // Aplicar la conversión si la moneda es diferente de pesos mexicanos
      const precioUnitario = moneda === 'pesos' ? item.Costo + iva  : ((item.Costo*this.conversionRate) + iva );
      const precioTotalSD = precioUnitario * item.Cantidad;
      const Desc = precioTotalSD*(item.Desct/100)
      const precioTotalCD=precioTotalSD-Desc;
      //console.log(precioTotalCD, item.Costo*this.conversionRate );
      const precioUnitario2=precioUnitario-precioUnitario*(item.Desct/100);
      //const precioTotalCD = precioUnitario2 * item.Cantidad;
      //this.margen = 1 - ((item.Costo*this.conversionRate) / precioUnitario2);
      item.VA = iva;
      item.PrecioUnitario = precioUnitario;
      item.Flete = (3.5/100)*precioTotalCD;
      item.PrecioTotal = precioTotalCD+item.Flete;
      const PrecioPoruno= item.PrecioTotal/item.Cantidad
      this.margen = 1 - ((item.Costo*this.conversionRate) / PrecioPoruno);
      item.Margen = this.margen;
      // Verificar si algún producto tiene margen menor a 0.29
      const algunProductoConMargenBajo = this.productos.some((producto: any) => producto.Margen < 0.29);
      // Actualizar la propiedad mostrarSolicitarAprobacion
      this.mostrarSolicitarAprobacion = algunProductoConMargenBajo;
      this.totalPrecioTotal = this.productos.reduce((total: number, producto: any) => total + producto.PrecioTotal,0);
    }
    
    else{
    const iva = (item.Costo * (this.ivaPorProducto[i] / 100 || 0.29 )) * this.conversionRate; // Aplicar la conversión al precio antes de calcular el IVA
    const flete = (fleteToShow* this.conversionRate) / item.Cantidad;
    // Aplicar la conversión si la moneda es diferente de pesos mexicanos
    const precioUnitario = moneda === 'pesos' ? item.Costo + iva + flete : ((item.Costo*this.conversionRate) + iva + flete);
    const precioTotalSD = precioUnitario * item.Cantidad;
    const Desc = precioTotalSD*(item.Desct/100)
    const precioTotalCD=precioTotalSD-Desc;
    //console.log(precioTotalCD, item.Costo*this.conversionRate );
    const precioUnitario2=precioUnitario-precioUnitario*(item.Desct/100);
    //const precioTotalCD = precioUnitario2 * item.Cantidad;
    this.margen = 1 - ((item.Costo*this.conversionRate) / precioUnitario2);
    item.VA = iva;
    item.Flete = flete;
    item.PrecioUnitario = precioUnitario;
    item.PrecioTotal = precioTotalCD;
    //item.Desct = Desc;
    item.Margen = this.margen;
    // Verificar si algún producto tiene margen menor a 0.29
    const algunProductoConMargenBajo = this.productos.some((producto: any) => producto.Margen < 0.29);
    // Actualizar la propiedad mostrarSolicitarAprobacion
    this.mostrarSolicitarAprobacion = algunProductoConMargenBajo;
    this.totalPrecioTotal = this.productos.reduce((total: number, producto: any) => total + producto.PrecioTotal,0);
  }
    return this.margen;
  }
  
  public totalCotizacion:any
  
  aplicarDescuento() {
    // Asumiendo que DescT es un porcentaje (por ejemplo, 10 para un 10% de descuento)
    console.log(this.DescT);
    const descuentoPorcentaje = this.DescT;
    // Calcula el descuento en función del porcentaje ingresado
    const descuento = (descuentoPorcentaje / 100) * this.totalPrecioTotal;
    // Aplica el descuento al totalPrecioTotal
    this.totalPrecioTotal -= descuento;
    this.totalCotizacion=this.totalPrecioTotal
  }
  
  validarInput(event: any) {
    const input = event.target;
    const valor = parseFloat(input.value);
    if (isNaN(valor)) {
      input.value = ''; // Establece el valor a vacío si no es un número positivo
    }
  }
  


  getvendedores(){
    this._service.vendedores().subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.vendedores = resp;
        //console.log(resp);
    }
    })
  }

  getGerentes(){
    this._service.gerentes().subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.gerentes = resp;
        //console.log(resp);
    }
    })
  }

  getClientes(){
    this._service.clientes().subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.clientes = resp;
        //console.log(resp);
    }
    })
  }

  getProductosCotizados(){
    const numeroCotizacion = localStorage.getItem('numeroCotizacion');    
    this._service.obtenerProductosCotizados(numeroCotizacion).subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.productos = resp;
        //console.log(resp);
    }
    })
  }

  
  getProductosCotizados2(){
    this.productos = this._serviceC.obtenerProductos(); // Obtener la lista de productos
    const productosGuardados = localStorage.getItem('productos');
    //console.log(productosGuardados);
    
    if (productosGuardados) {
      this.productos = JSON.parse(productosGuardados);
    }
  }



  //guardar datos de cotizacion e localstorage
  guardar(){
    if ( this.datosCotizacion.Clientes_ID_Cliente=='' ||  this.datosCotizacion.Usuario_Clave_usuario=='' ||  this.datosCotizacion.Comentarios=='' ||  this.flete==''   ) {
      alert("Ingresa los datos para tu cotización")
    } else {
      localStorage.setItem('FechaEmision', this.fecha);
      localStorage.setItem('FechaExpiracion', this.datosCotizacion.FechaExpiracion);
      localStorage.setItem('Total', this.datosCotizacion.Total);
      localStorage.setItem('Clientes_ID_Cliente', this.datosCotizacion.Clientes_ID_Cliente);
      localStorage.setItem('Usuario_Clave_usuario', this.datosCotizacion.Usuario_Clave_usuario);
      localStorage.setItem('Comentarios', this.datosCotizacion.Comentarios);
      localStorage.setItem('Flete2', this.flete);
      localStorage.setItem('Flete', this.fleteToShow.toString());
      localStorage.setItem('Moneda', this.moneda);
      //localStorage.setItem('ManualF', this.ManualFlete);

      alert("Datos guardados temporalmente")
    }
  }


  /*
  //obtener total de cotizacion
  getSumaPreciosTotales(): number {
    return this.totalPrecioTotal;
  }*/

  productosAgregados: any[] = [];

  //insertar cotizacion
  public productosA:any
  public datosCoti:any
  public DatosU:any
  public datosCliente:any
  public TotalDeCotizacion:any

  crearCotizacionYProductos() {
    const productos = this._serviceC.obtenerProductos(); // Obtener la lista de productos
    console.log(productos);
    if (this.DescT==0){
      this.TotalDeCotizacion=this.totalPrecioTotal
    }
    else if (this.DescT!=0){
      this.TotalDeCotizacion=this.totalCotizacion
    }
    const datosCotizacionProductos = {
      FechaEmision: this.fecha,
      FechaExpiracion: this.fechaE,
      Clientes_ID_Cliente: this.datosCotizacion.Clientes_ID_Cliente,
      Usuario_Clave_usuario:  this.datosCotizacion.Usuario_Clave_usuario,
      Comentarios:  this.datosCotizacion.Comentarios,
      Status:'Aprobada',
      Productos: productos,
      Total:this.TotalDeCotizacion

    };
    if (this.flete=='Seleccione flete' || datosCotizacionProductos.Productos.length==0 || this.datosCotizacion.Clientes_ID_Cliente=='' || this.datosCotizacion.Usuario_Clave_usuario=='' || this.datosCotizacion.Comentarios=='') {
      alert("Es necesario completar la información en tu cotización")
    } else {
      this._service.crearCotizacionYProductos(datosCotizacionProductos).subscribe((data:any)=>{//LLAMA AL SERVICIO PARA AGREGAR AL USUARIO
    
        if (data.message == 'Datos insertados exitosamente') {
          Swal.fire('Éxito', 'La cotizacion se ha guardado con éxito', 'success');
          this.datosCoti=data.cotizacion;
          this.productosA=data.productos;
          this.DatosU=data.datosU;
          this.datosCliente=data.razonSocialCliente;

            //console.log(datosCotizacionProductos);
        } else {
          Swal.fire('Error', 'Ocurrió un error inesperado', 'error');
          //alert("Error al registrar");
        }
      });
      localStorage.removeItem("FechaEmision");
      localStorage.removeItem("FechaExpiracion");
      localStorage.removeItem("Total");
      localStorage.removeItem("Clientes_ID_Cliente");
      localStorage.removeItem("Usuario_Clave_usuario");
      localStorage.removeItem("Comentarios");
      localStorage.removeItem("Flete");
      localStorage.removeItem("Flete2");
      localStorage.removeItem("Moneda");    
      //window.location.reload()
    }
  }


  aprobacion={Aprobador:'', Comentarios:''}


    //insertar cotizacion pero espera aprobacion
    crearCotizacionYProductosConAprobacion() {
      const productos = this._serviceC.obtenerProductos(); // Obtener la lista de productos
      const datosCotizacionProductos = {
        FechaEmision: this.fecha,
        FechaExpiracion: this.fechaE,
        Clientes_ID_Cliente: this.datosCotizacion.Clientes_ID_Cliente,
        Usuario_Clave_usuario:  this.datosCotizacion.Usuario_Clave_usuario,
        Comentarios:  this.datosCotizacion.Comentarios,
        Status:'Revisar',
        Productos: productos,
        Total:this.totalPrecioTotal,
        Aprobador: this.gerentes[0].Clave_usuario,
        RealizadorCotizacion: this.datosCotizacion.Usuario_Clave_usuario,
        Comentarios2:  this.aprobacion.Comentarios,
      };
      if (this.flete=='' || datosCotizacionProductos.Productos.length==0 || this.datosCotizacion.Usuario_Clave_usuario=='' ||  this.datosCotizacion.Clientes_ID_Cliente=='' ) {
        alert("Falta ingresar datos a tu cotización")
      } else {
        //console.log("APROBADOR",datosCotizacionProductos.Aprobador);
        
        this._service.EnviarAprobacion(datosCotizacionProductos).subscribe((data:any)=>{//LLAMA AL SERVICIO PARA AGREGAR AL USUARIO
      
          if (data.message == 'Datos insertados exitosamente') {
            alert("Se envió cotización para revisión");
              //console.log(datosCotizacionProductos);
          } else {
            alert("Error al registrar");
          }
        });
        localStorage.removeItem("FechaEmision");
        localStorage.removeItem("FechaExpiracion");
        localStorage.removeItem("Total");
        localStorage.removeItem("Clientes_ID_Cliente");
        localStorage.removeItem("Usuario_Clave_usuario");
        localStorage.removeItem("Comentarios");
        localStorage.removeItem("Flete");
        localStorage.removeItem("Flete2");
        localStorage.removeItem("Moneda");    
        //window.location.reload()
  
      }
    }


  EnviarAprobacion() {
    const DatosAprobacion = {
      Aprobador: this.aprobacion.Aprobador,
      RealizadorCotizacion: this.datosCotizacion.Usuario_Clave_usuario,
      Comentarios:  this.aprobacion.Comentarios,
    };
    //console.log(DatosAprobacion.Aprobador);
      this._service.EnviarAprobacion(DatosAprobacion).subscribe((data:any)=>{//LLAMA AL SERVICIO PARA AGREGAR AL USUARIO
        if (data.message == 'Datos insertados exitosamente') {
          alert("Registrado exitosamente");
            //console.log(datosCotizacionProductos);
        } else {
          alert("Error al registrar");
        }
        
      });
    }
  
  
  AgregarUser(){
    this.guardar()
       if(this.datosCotizacion.FechaExpiracion==''){
      window.alert("Debes ingresar todos los datos solicitados")
    }
    else{
      //console.log(this.datosCotizacion);//DATOS REGISTRADOS MOSTRADOS EN CONSOLA
      this._service.agregarCotizacion(this.datosCotizacion).subscribe((data:any)=>{//LLAMA AL SERVICIO PARA AGREGAR AL USUARIO
      this.numC=data.cotizacionId
      localStorage.setItem('numeroCotizacion', this.numC.toString());
      if (data.message == 'Datos insertados exitosamente') {
        alert("Registrado exitosamente");
          //console.log(this.datosCotizacion);
      } else {
        alert("Error al registrar");
      }
      
    })}
  }



  
  eliminarlocal(){

  const answer = window.confirm("¿Estás seguro de salir? Se perderan tus datos");
  if (answer) {
    localStorage.removeItem("FechaEmision");
    localStorage.removeItem("FechaExpiracion");
    localStorage.removeItem("Total");
    localStorage.removeItem("Clientes_ID_Cliente");
    localStorage.removeItem("Usuario_Clave_usuario");
    localStorage.removeItem("Comentarios");
    localStorage.removeItem("Flete");
    localStorage.removeItem("Flete2");
    localStorage.removeItem("productos");
    localStorage.removeItem("Moneda");
    this.productos.length=0;
    this._router.navigate(['menu']);
    }
  }

  
  //eliminar datos  de cotizacion del localhost
  eliminarDatosCoti(){
    localStorage.removeItem("FechaEmision");
    localStorage.removeItem("FechaExpiracion");
    localStorage.removeItem("Total");
    localStorage.removeItem("Clientes_ID_Cliente");
    localStorage.removeItem("Usuario_Clave_usuario");
    localStorage.removeItem("Comentarios");
    localStorage.removeItem("Flete");
    localStorage.removeItem("Flete2");
    this.datosCotizacion.Usuario_Clave_usuario='';
    this.datosCotizacion.Clientes_ID_Cliente='';
    this.datosCotizacion.FechaExpiracion='';
    this.datosCotizacion.Comentarios='';
    this.flete='';
    this.ManualFlete='';
  }



  public Datos3:any
  GenerarPDF(){ 
    //console.log(this.datosCoti);

    if (this.DescT==0){
      this.TotalDeCotizacion=this.totalPrecioTotal
    }
    else if (this.DescT!=0){
      this.TotalDeCotizacion=this.totalCotizacion
    }

    if(this.datosCoti==null){
      alert("Antes de obtener el PDF, asegúrate de guardar la cotización");
      
    }else{
    const doc = new jsPDF({ unit: 'mm', format: [215.9, 279.4] }); // Tamaño personalizado en milímetros (215.9 mm x 279.4 mm)
    doc.setFontSize(9); 
    doc.rect(167.30, 2.76, 45.95, 21);
    doc.setFont("Helvetica", "bold");
    doc.text('Código: FO06-VEN-01', 173, 6.26);
    doc.text('Emisión: 03/03/2023', 173, 10.16);
    doc.text('Revisión: 01', 173, 14.94);
    doc.text('Página: 1 de 4', 173, 19.24);
    doc.rect(167.30, 2.76, 4, 21, 'F'); // 'F' indica que el rectángulo debe ser relleno


    const imgData =  "assets/img/ImgCoti/persona.jpg";
    //x,y,ancho, alto
    doc.addImage(imgData, 'JPEG',28.6, 60, 161 , 172); 

    doc.setFontSize(39.5); 
    doc.text('PROPUESTA COMERCIAL', 17.66, 43);
    doc.rect(0, 184.18, 215.9, 95, 'F');

    const imgData2 =  "assets/img/ImgCoti/logo.png";
    doc.addImage(imgData2, 'PNG',85.17, 214.20, 44.7 , 41.7); 
    doc.setFontSize(13); 
    doc.setFont("Helvetica", "bold");
    doc.setTextColor(255, 255, 255);
    doc.text('Contáctanos', 32.06, 234.7);
    doc.setFontSize(12); 
    doc.text('soporte@parautos.com.mx', 16.08, 244.3);
    doc.text('7775006144', 16.08, 254.12);
    doc.text('www.parautos.com.mx', 16.08, 263.8);

    doc.setFontSize(13); 
    doc.text('TAKE THE WORK OUT', 149.4, 251.6);
    doc.text('OF YOUR WORKDAY', 149.4, 259.05);

    doc.addPage();
    doc.rect(0, 0, 21.94, 279.4, 'F'); // 'F' indica que el rectángulo debe ser relleno
    doc.setTextColor(0, 0, 0);
    doc.setFontSize(45.5); 
    doc.text('Nosotros', 73.58, 24.32);
    doc.setFontSize(12); 
    doc.text('Durante más de 20 años, seguimos enorgulleciéndonos de brindar atención', 34.54, 35.68);
    doc.text('y apoyo a nuestros clientes, quienes gozan de beneficios EXCLUSIVOS tales', 34.54, 42.95);
    doc.text('como: Educación digital, Soporte gratuito y diagnóstico empresarial.', 41.56, 49.18);

    doc.text('Contamos con +7 líneas de negocio cuidadosamente seleccionadas con el', 34.54, 60);
    doc.text('fin de aportar a tu empresa beneficios y soluciones exclusivas.', 47.11, 66.55);

    const anticorro =  "assets/img/ImgCoti/anticorrosivos.jpg";
    doc.addImage(anticorro, 'JPEG',46.76, 76.55, 61.65 , 60.96); 
    doc.setFontSize(20); 

    const backgroundColor = '#FFFFFF'; 
    doc.setFillColor(backgroundColor);
    doc.rect(45.46, 104.12, 70.68, 9.16, 'F');
    doc.text('ANTICORROSIVOS', 45.76, 112);


    const lubri =  "assets/img/ImgCoti/lubricantes.jpg";
    doc.addImage(lubri, 'JPEG',116.69, 76.55, 61.65 , 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(116.69, 104.12, 70.68, 9.16, 'F');
    doc.text('LUBRICANTES', 120.69, 112);
    

    const liq =  "assets/img/ImgCoti/liquidoM.jpg";
    doc.addImage(liq, 'JPEG',46.76, 145.32, 61.65 , 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(46.76, 173.7, 70.68, 9.16, 'F');
    doc.text('LIQ. MAQUINADO', 47.69, 181.3);
    

    const hotmelt =  "assets/img/ImgCoti/Hotmelts.jpg";
    doc.addImage(hotmelt, 'JPEG',116.69, 145.32, 61.65 , 60.96); 
    doc.setFillColor(backgroundColor);
    doc.rect(116.69, 173.7, 70.68, 9.16, 'F');
    doc.text('HOTMELTS', 126.69, 181.3);


    const pla =  "assets/img/ImgCoti/plastisol.jpg";
    doc.addImage(pla, 'JPEG',46.76, 213.08, 61.65 , 60.96); 
    doc.setFillColor(backgroundColor);
    doc.rect(46.76, 244.75, 70.68, 9.16, 'F');
    doc.text('PLASTISOLES', 51.12, 252.75);

    const erp =  "assets/img/ImgCoti/ERP.jpg";
    doc.addImage(erp, 'JPEG',116.69, 213.08, 61.65 , 60.96); 
    doc.setFillColor(backgroundColor);
    doc.rect(116.69, 244.75, 70.68, 9.16, 'F');
    doc.text('ERP', 138.19, 252.75);

    doc.addPage();
    doc.setFontSize(45.5); 
    doc.text('Cotización', 28.16, 26.46);
    const backgroundColorN = '#393939'; 
    doc.setFillColor(backgroundColorN);


    const Logo2 =  "assets/img/ImgCoti/parautosN.png";
    doc.addImage(Logo2, 'PNG',184.11, 6, 27.77 , 26.77); 
    doc.rect(28.91, 46.75, 46.36, 23.44);
    doc.rect(28.91, 46.75, 3.98, 23.44, 'F');

    doc.rect(84.14, 46.75, 46.36, 23.44);
    doc.rect(84.14, 46.75, 3.98, 23.44, 'F');

    doc.rect(140.15, 46.75, 46.36, 23.44);
    doc.rect(140.15, 46.75, 3.98, 23.44, 'F');

    doc.setFontSize(10.5); 
    doc.text('Cotización', 38.12, 51.61);
    doc.text('Fecha', 104.40, 51.61);
    doc.text('Expiración', 153.88, 51.61);
    const cadena = this.datosCoti.NumCotizacion.toString();
    doc.setFontSize(21); 
    doc.text("#"+cadena, 40.53, 60.29);
    const productos = this.productosA;

    
    //console.log(productos);
    const startX = 29.09; // Coordenada X
    const startY = 97.44; // Coordenada Y
    const data2 = [] as [number, string, number, number][];
      //console.log(productos);
      
    productos.forEach((data:any) => {
      const precioUnitario = data.PrecioUnitario.toFixed(3); // Formatear a 2 decimales
      data2.push([data.Cantidad, data.Descripcion, data.Unidad_Medida, precioUnitario ]);
    });

    //console.log(data2.length);
    
    const tableHeight = data2.length * 12; // Suponiendo 12 puntos de altura por fila
    const marginLeft = 28.56; // Ajusta el valor según el margen deseado

    autoTable(doc, {
      headStyles: { fillColor: [57, 57, 57] }, 
      head: [['EUA', 'Numero de parte', 'U/M', 'Precio Unitario']], 
      body: data2,
      startY: startY,
      tableWidth: 158, // Especificar el ancho de la tabla
      margin: {left: marginLeft }, // Especificar el margen izquierdo

      
    })
    const textY = tableHeight + 110.33; 
    doc.setFont("Helvetica", "normal");

    doc.setFontSize(10); 
    doc.text('El precio es en dólares americanos más IVA', 34.64, textY);
    doc.text('El precio es de acuerdo a la cantidad mínima', 34.64, textY+5.54);
    doc.text('marcada para cada material, si es menor', 34.64, textY+11.08);
    doc.text('cantidad el precio deberá de incrementarse.', 34.64, textY+16.64);

    doc.setFontSize(11); 

    doc.rect(137.39, textY, 22.17, 13.70, 'F');
    doc.setTextColor(255, 255, 255);
    doc.text('TOTAL', 142.46, textY+5.54);
    doc.rect(159.79, textY, 22.17, 13.70);
    doc.setTextColor(0,0,0);
    doc.setFontSize(9); 

    const precioTotalFormateado =  parseFloat(this.TotalDeCotizacion.toFixed(2));
    doc.text('$'+precioTotalFormateado, 161.95, textY+5.54);

    // Dibujar una línea horizontal
    const xInicio = 28.56; // Coordenada x del punto de inicio
    const yInicio = textY+21.64; // Coordenada y del punto de inicio
    const xFin = 186.62;  // Coordenada x del punto de final
    const yFin = textY+21.64; ;   // Coordenada y del punto de final
    doc.setDrawColor(161, 161, 161); // Rojo en formato RGB
    doc.line(xInicio, yInicio, xFin, yFin);

    doc.rect(28.56, textY+33.12, 70.89, 52.49);
    doc.setFontSize(11); 

    doc.text('Contacto', 53.02, textY+36.64);
    doc.text('Observaciones', 132.31, textY+36.64);

    doc.setFontSize(9.5); 
    doc.setFont("Helvetica", "italic");
    doc.text('A partir de la segunda venta con OC abierta por parte', 103, textY+42.64);
    doc.text('del cliente, el tiempo de entrega es de 1 semana de acuerdo', 103, textY+47.64);
    doc.text('con los requerimientos del cliente, contando con', 103, textY+53.24);
    doc.text('un stock de seguridad de un mes en Parautos.', 103, textY+58.54);

    doc.text('Si la entrega es urgente, el precio aumenta debido a los', 103, textY+68.24);
    doc.text('costos de transportación.', 103, textY+73.54);

    doc.text('Capacitación mensual y seminario de la corrosión SIN costo', 103, textY+83.24);

    //doc.text(this.DatosU[0].Clave_usuario, 60, textY+47.64);
    doc.text(this.DatosU[0].Nombre, 60, textY+47.64);
    doc.text(this.DatosU[0].ApellidoP, 60, textY+55.34);
    doc.text(this.DatosU[0].Rol, 60, textY+63.54);
    doc.text(this.DatosU[0].Telefono, 60, textY+71.24);
    doc.text(this.DatosU[0].email, 32.64, textY+78.94);

    console.log(this.DatosU[0].imagen);
    const rutaImagen = this.DatosU[0].imagen; // Obtén esta ruta de tu base de datos
    this.urlImagen = `${URL}${rutaImagen}`;
    console.log(this.urlImagen);

    const vendedor =  this.urlImagen;
    doc.addImage(vendedor, 'PNG', 35, textY+44, 20, 26.55);

    
    doc.setFont("Helvetica", "bold");
    const fechaParseada = new Date(this.fecha);
    const fechaE = format(fechaParseada, 'dd/MM/yyyy');
    //console.log("fecha", fechaE);
    doc.setFontSize(21); 
    doc.text(fechaE, 89.53, 60.29);

    
    const fechaParseada2 = new Date(this.fechaE);
    const fechaEX = format(fechaParseada2, 'dd/MM/yyyy');
    const datosCliente2 = this.datosCliente;
    const datosC = datosCliente2.charAt(0).toUpperCase() + datosCliente2.slice(1).toLowerCase();

    doc.text(fechaEX, 145.30, 60.29);
    doc.setFillColor(backgroundColorN);

    doc.setFontSize(7); 
    const tableHeight2 = datosC.length * 7; // Suponiendo 12 puntos de altura por fila
    const marginLeft2 = 28.56; // Ajusta el valor según el margen deseado
    const data = [
      [datosC, this.datosCotizacion.Comentarios , datosC, this.datosCotizacion.Comentarios],
    ];
    autoTable(doc, {
      headStyles: { fillColor: [57, 57, 57] }, 
      head: [['Referencia', 'Vía de embarque', 'Lab', 'Terminos']], 
      body: data,
      startY: 71,
      tableWidth: 158, // Especificar el ancho de la tabla
      margin: {left: marginLeft2 }, // Especificar el margen izquierdo

      
    })

    /*
    doc.text("Referencia", 35.33, 78.66);
    doc.setTextColor(0, 0, 0);
    doc.setFontSize(5); 
    doc.text(datosCliente2, 35.33, 88.66);
    doc.setFontSize(12); 
    doc.setTextColor(255, 255, 255);
    doc.text("Vía de embarque", 74.12, 78.66);
    doc.text("Lab", 130.93, 78.66);
    doc.setTextColor(0, 0, 0);
    doc.setFontSize(5); 
    doc.text(datosCliente2,130.93, 88.66);
    doc.setTextColor(255, 255, 255);
    doc.setFontSize(12); 
    doc.text("Terminos", 162.79, 78.66);
    */

    // Guarda el PDF
    doc.output('dataurlnewwindow')
    this.modalService.dismissAll()
    //doc.save('COTIZACION.pdf');
  }
}




public urlImagen:any
  GenerarPDF2() {
    if (this.DescT==0){
      this.TotalDeCotizacion=this.totalPrecioTotal
    }
    else if (this.DescT!=0){
      this.TotalDeCotizacion=this.totalCotizacion
    }
    if(this.datosCoti==null){
      alert("Antes de obtener el PDF, asegúrate de guardar la cotización");
    }else{
    const doc = new jsPDF({ unit: 'mm', format: [215.9, 279.4] }); // Custom size in millimeters (215.9 mm x 279.4 mm)
    doc.setFontSize(9);
    doc.rect(167.30, 2.76, 45.95, 21);
    doc.setFont("Helvetica", "bold");
    doc.text('Code: FO06-VEN-01', 173, 6.26);
    doc.text('Issue Date: 03/03/2023', 173, 10.16);
    doc.text('Revision: 01', 173, 14.94);
    doc.text('Page: 1 of 4', 173, 19.24);
    doc.rect(167.30, 2.76, 4, 21, 'F'); // 'F' indicates that the rectangle should be filled
  
    const imgData = "assets/img/ImgCoti/persona.jpg";
    // x, y, width, height
    doc.addImage(imgData, 'JPEG', 28.6, 60, 161, 172);
  
    doc.setFontSize(39.5);
    doc.text('BUSINESS PROPOSAL', 17.66, 43);
    doc.rect(0, 184.18, 215.9, 95, 'F');
  
    const imgData2 = "assets/img/ImgCoti/logo.png";
    doc.addImage(imgData2, 'PNG', 85.17, 214.20, 44.7, 41.7);
    doc.setFontSize(13);
    doc.setFont("Helvetica", "bold");
    doc.setTextColor(255, 255, 255);
    doc.text('Contact Us', 32.06, 234.7);
    doc.setFontSize(12);
    doc.text('support@parautos.com.mx', 16.08, 244.3);
    doc.text('7775006144', 16.08, 254.12);
    doc.text('www.parautos.com.mx', 16.08, 263.8);
  
    doc.setFontSize(13);
    doc.text('TAKE THE WORK OUT', 149.4, 251.6);
    doc.text('OF YOUR WORKDAY', 149.4, 259.05);
  
    doc.addPage();
    doc.rect(0, 0, 21.94, 279.4, 'F'); // 'F' indicates that the rectangle should be filled
    doc.setTextColor(0, 0, 0);
    doc.setFontSize(45.5);
    doc.text('About Us', 73.58, 24.32);
    doc.setFontSize(12);
    doc.text('For more than 20 years, we continue to pride ourselves on providing care', 40.38, 35.68);
    doc.text('and support to our customers, who enjoy EXCLUSIVE benefits such as:', 34.54, 42.95);
    doc.text('Digital Education, Free Support & Business Diagnostics.', 47.65, 49.18);
  
    doc.text('We have +7 carefully selected lines of business in order to provide your ', 40.05, 60);
    doc.text('company with exclusive benefits and solutions.', 53.05, 66.55);
  
    const anticorro = "assets/img/ImgCoti/anticorrosivos.jpg";
    doc.addImage(anticorro, 'JPEG', 46.76, 76.55, 61.65, 60.96);
    doc.setFontSize(20);
  
    const backgroundColor = '#FFFFFF';
    doc.setFillColor(backgroundColor);
    doc.rect(45.46, 104.12, 70.68, 9.16, 'F');
    doc.setFontSize(15);
    doc.text('CORROSION SOLUTIONS', 35.76, 112);
    doc.setFontSize(20);
    const lubri = "assets/img/ImgCoti/lubricantes.jpg";
    doc.addImage(lubri, 'JPEG', 116.69, 76.55, 61.65, 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(116.69, 104.12, 70.68, 9.16, 'F');
    doc.text('LUBRICANTS', 120.69, 112);
  
    const liq = "assets/img/ImgCoti/liquidoM.jpg";
    doc.addImage(liq, 'JPEG', 46.76, 145.32, 61.65, 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(46.76, 173.7, 70.68, 9.16, 'F');
    doc.text('MACHINED LIQUID', 47.69, 181.3);
  
    const hotmelt = "assets/img/ImgCoti/Hotmelts.jpg";
    doc.addImage(hotmelt, 'JPEG', 116.69, 145.32, 61.65, 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(116.69, 173.7, 70.68, 9.16, 'F');
    doc.text('HOTMELTS', 126.69, 181.3);
  
    const pla = "assets/img/ImgCoti/plastisol.jpg";
    doc.addImage(pla, 'JPEG', 46.76, 213.08, 61.65, 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(46.76, 244.75, 70.68, 9.16, 'F');
    doc.text('PLASTISOLS', 51.12, 252.75);
  
    const erp = "assets/img/ImgCoti/ERP.jpg";
    doc.addImage(erp, 'JPEG', 116.69, 213.08, 61.65, 60.96);
    doc.setFillColor(backgroundColor);
    doc.rect(116.69, 244.75, 70.68, 9.16, 'F');
    doc.text('ERP', 138.19, 252.75);
  
    doc.addPage();
    doc.setFontSize(45.5);
    doc.text('Quote', 28.16, 26.46);
    const backgroundColorN = '#393939';
    doc.setFillColor(backgroundColorN);
  
    const Logo2 = "assets/img/ImgCoti/parautosN.png";
    doc.addImage(Logo2, 'PNG', 184.11, 6, 27.77, 26.77);
    doc.rect(28.91, 46.75, 46.36, 23.44);
    doc.rect(28.91, 46.75, 3.98, 23.44, 'F');
  
    doc.rect(84.14, 46.75, 46.36, 23.44);
    doc.rect(84.14, 46.75, 3.98, 23.44, 'F');
    doc.rect(140.15, 46.75, 46.36, 23.44);
  doc.rect(140.15, 46.75, 3.98, 23.44, 'F');

  doc.setFontSize(10.5);
  doc.text('Quote', 38.12, 51.61);
  doc.text('Date', 104.40, 51.61);
  doc.text('Exp', 153.88, 51.61);
  const reference = this.datosCoti.NumCotizacion.toString();
  doc.setFontSize(21);
  doc.text("#" + reference, 40.53, 60.29);
  const products = this.productosA;
  //console.log(products);
  const startX = 29.09; // X coordinate
  const startY = 97.44; // Y coordinate
  const data2 = [] as [number, string, number, number][];

  products.forEach((data: any) => {
    const unitPrice = data.PrecioUnitario.toFixed(2); // Format to 2 decimal places
    data2.push([data.Cantidad, data.Descripcion, data.Unidad_Medida, unitPrice]);
  });

  //console.log(data2.length);

  const tableHeight = data2.length * 12; // Assuming 12 points of height per row
  const marginLeft = 28.56; // Adjust the value as per the desired margin

  autoTable(doc, {
    headStyles: { fillColor: [57, 57, 57] },
    head: [['EAU', 'Part Number', 'U/M', 'Unit Price']],
    body: data2,
    startY: startY,
    tableWidth: 158, // Specify the table width
    margin: { left: marginLeft },
  });

  const textY = tableHeight + 110.33;
  doc.setFont("Helvetica", "normal");

  doc.setFontSize(10);
  doc.text('The price is in US dollars + IVA', 34.64, textY);
  doc.text('The price is based on the minimum quantity', 34.64, textY + 5.54);
  doc.text('marked for each material, if it´s less', 34.64, textY + 11.08);
  doc.text('than the minimum quantity, the price should be increased.', 34.64, textY + 16.64);

  doc.setFontSize(11);

  doc.rect(137.39, textY, 22.17, 13.70, 'F');
  doc.setTextColor(255, 255, 255);
  doc.text('TOTAL', 142.46, textY + 5.54);
  doc.rect(159.79, textY, 22.17, 13.70);
  doc.setTextColor(0, 0, 0);
  doc.setFontSize(9);

  const formattedTotalPrice =  parseFloat(this.TotalDeCotizacion.toFixed(2));
  doc.text('$' + formattedTotalPrice, 161.95, textY + 5.54);

  // Draw a horizontal line
  const xStart = 28.56; // x-coordinate of the starting point
  const yStart = textY + 21.64; // y-coordinate of the starting point
  const xEnd = 186.62; // x-coordinate of the ending point
  const yEnd = textY + 21.64; // y-coordinate of the ending point
  doc.setDrawColor(161, 161, 161); // Red in RGB format
  doc.line(xStart, yStart, xEnd, yEnd);

  doc.rect(28.56, textY + 33.12, 70.89, 52.49);
  doc.setFontSize(11);

  doc.text('Contact', 53.02, textY + 36.64);
  doc.text('Remarks', 132.31, textY + 36.64);

  doc.setFontSize(9.5);
  doc.setFont("Helvetica", "italic");
  doc.text('From the second sale with an open PO by the customer,', 103, textY + 42.64);
  doc.text('the delivery time is 1 week according to ', 103, textY + 47.64);
  doc.text('customer requirements, with a one-month safety stock in Parautos.', 103, textY + 53.24);
  doc.text('If the delivery is urgent, the price increases due to transportation costs.', 103, textY + 58.54);

  doc.text('Monthly corrosion training and seminar free', 103, textY + 68.24);

  // doc.text(this.DatosU[0].Clave_usuario, 60, textY+47.64);
  doc.text(this.DatosU[0].Nombre, 60, textY+47.64);
  doc.text(this.DatosU[0].ApellidoP, 60, textY+55.34);
  doc.text(this.DatosU[0].Rol, 60, textY+63.54);
  doc.text(this.DatosU[0].Telefono, 60, textY+71.24);
  doc.text(this.DatosU[0].email, 32.64, textY+78.94);

  //console.log(this.DatosU[0].imagen);
  const rutaImagen = this.DatosU[0].imagen; // Obtén esta ruta de tu base de datos
  this.urlImagen = `${URL2}${rutaImagen}`;
  //console.log(this.urlImagen);
  
  const vendedor =  this.urlImagen;
  doc.addImage(vendedor, 'PNG', 35, textY+44, 20, 26.55);


  doc.setFont("Helvetica", "bold");
  const formattedDate = new Date(this.fecha);
  const dateStr = format(formattedDate, 'dd/MM/yyyy');
  //console.log("date", dateStr);
  doc.setFontSize(21);
  doc.text(dateStr, 89.53, 60.29);

  const formattedExpirationDate = new Date(this.fechaE);
  const expirationDateStr = format(formattedExpirationDate, 'dd/MM/yyyy');
  doc.text(expirationDateStr, 145.30, 60.29);
  const datosC=this.datosCliente
  doc.setFontSize(7); 
  const tableHeight2 = datosC.length * 7; // Suponiendo 12 puntos de altura por fila
  const marginLeft2 = 28.56; // Ajusta el valor según el margen deseado
  const data = [
    [datosC, this.datosCotizacion.Comentarios , datosC, this.datosCotizacion.Comentarios],
  ];
  autoTable(doc, {
    headStyles: { fillColor: [57, 57, 57] }, 
    head: [['Referencia', 'Vía de embarque', 'Lab', 'Terminos']], 
    body: data,
    startY: 71,
    tableWidth: 158, // Especificar el ancho de la tabla
    margin: {left: marginLeft2 }, // Especificar el margen izquierdo

    
  })


  /*
  doc.setFillColor(backgroundColorN);
  doc.rect(29.09, 72.97, 160.02, 9.47, 'F');
  doc.setFontSize(12);
  doc.setTextColor(255, 255, 255);
  doc.text("Reference", 35.33, 78.66);
  doc.text("Shipping Method", 74.12, 78.66);
  doc.text("Lab", 130.93, 78.66);
  doc.text("Terms", 162.79, 78.66);
  */

  // Save the PDF
  doc.output('dataurlnewwindow')
  this.modalService.dismissAll()

}
}

  cerrarModalIdioma(){
    this.modalService.dismissAll()

  }
  


  }
    

  

