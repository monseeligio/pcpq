import { Component } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import { CotizacionService } from 'src/app/services/serviceUsurio/cotizacion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vci',
  templateUrl: './vci.component.html',
  styleUrls: ['./vci.component.css']
})
export class VciComponent {
  constructor(private _service:UsuarioService, private productosService:CotizacionService, private _router: Router){}
  public cantidad:any
  public role:any


  ngOnInit(): void {
    this.getProductos()
    this.role=localStorage.getItem("rol")
  }
  public productos:any;
  datosProductos={Productos_NumArticulo:'', Descripcion:'',Cotizacion_NumCotizacion:'', Cantidad:'', Unidad_Medida:'', Costo:'',Flete: '', Precio:'', VA:'', Desct:'', PrecioUnitario:'', PrecioTotal:'', Margen:''}

  getProductos(){
    this._service.ProductosV().subscribe((resp:any)=>{
      if (Array.isArray(resp)) { 
        this.productos = resp;
        ////console.log(resp);
    }
    })
  }

  agregarProducto(producto:any) {
    this.datosProductos.Productos_NumArticulo=producto.NumArticulo
    this.datosProductos.Productos_NumArticulo=producto.NumArticulo
    this.datosProductos.Descripcion=producto.Descripcion
    this.datosProductos.Cantidad=producto.Cantidad
    this.datosProductos.Unidad_Medida=producto.Unidad_Medida
    ////console.log(producto);
    this.datosProductos.Costo=producto.Costo
    if(producto.Cantidad==null || producto.Cantidad=='0'){
      alert("Ingrese la cantidad de producto")
    }
    else{
    this.productosService.agregarProducto(this.datosProductos);
    this.datosProductos={Productos_NumArticulo:'', Descripcion:'',Cotizacion_NumCotizacion:'', Cantidad:'', Unidad_Medida:'', Costo:'',Flete: '', Precio:'', VA:'', Desct:'', PrecioUnitario:'', PrecioTotal:'', Margen:''}
    this._router.navigate(['cotizacion']);
    }
  }


  getImageUrl(imageId: any) {
    ////console.log("mostrar id",imageId);
    return this._service.getImagen(imageId)
  }

  imagenError(event: any) {
    ////console.log('Error al cargar la imagen:', event);
  }
  
  validarInput(event: any) {
    const input = event.target;
    input.value = input.value.replace(/[^0-9]/g, ''); // Elimina caracteres no numéricos
  }


}
