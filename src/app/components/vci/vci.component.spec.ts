import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VciComponent } from './vci.component';

describe('VciComponent', () => {
  let component: VciComponent;
  let fixture: ComponentFixture<VciComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VciComponent]
    });
    fixture = TestBed.createComponent(VciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
