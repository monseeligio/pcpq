import { Component } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  public role:any;
  public usuario:any;

  ngOnInit(): void {
   
    this.role = localStorage.getItem('rol');
    this.usuario = localStorage.getItem('usuario')

    //console.log('role '+this.role );
  }

  logout(){
    localStorage.removeItem("rol");
    localStorage.removeItem("token");

    localStorage.clear();
  }
}
