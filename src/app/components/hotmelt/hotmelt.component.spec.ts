import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HotmeltComponent } from './hotmelt.component';

describe('HotmeltComponent', () => {
  let component: HotmeltComponent;
  let fixture: ComponentFixture<HotmeltComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HotmeltComponent]
    });
    fixture = TestBed.createComponent(HotmeltComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
