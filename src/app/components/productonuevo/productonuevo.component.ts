import { Component } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';


@Component({
  selector: 'app-productonuevo',
  templateUrl: './productonuevo.component.html',
  styleUrls: ['./productonuevo.component.css']
})
export class ProductonuevoComponent {
  constructor(private _serviceM:UsuarioService){}

  public categoria:any
  datosArticulo={NumArticulo:'', Descripcion:'', Codigo_barras:'', Fabricante:'', Unidad_medida:'', imagen:'', Categoria_CodigoClasificacion:'', Costo:''}
  ngOnInit(): void {
    this.categorias()
  }

  categorias(){
    this._serviceM.categorias().subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.categoria = resp;
        //console.log(resp);
    }
    })
  }

  /*
  AgregarCliente() {
    //console.log(this.datosArticulo.Categoria_CodigoClasificacion);
    if (
      this.datosArticulo.NumArticulo == '' ||
      this.datosArticulo.Descripcion == '' ||
      this.datosArticulo.Unidad_medida == '' ||
      this.datosArticulo.Categoria_CodigoClasificacion == '' ||
      this.datosArticulo.Costo == ''
      ) {
      window.alert('Debes ingresar los datos solicitados');
    } 
    else{
      console.log(this.datosArticulo);
      
      this._serviceM.agregarProducto(this.datosArticulo).subscribe((data: any) => {
        if (data.message == 'Datos del producto insertados exitosamente') {
          alert('Registrado exitosamente');
        } else if (data.message == 'El producto con este NumArticulo ya existe') {
          alert('El producto con este Número de articulo ya existe');
        } else {
          alert('Error al registrar el usuario');
        }
      });
    }
  }
  */

  AgregarProdcutos() {
    if (
      this.datosArticulo.NumArticulo == '' ||
      this.datosArticulo.Descripcion == '' ||
      this.datosArticulo.Unidad_medida == '' ||
      this.datosArticulo.Categoria_CodigoClasificacion == '' ||
      this.datosArticulo.Costo == ''
    ) {
      window.alert('Debes ingresar los datos solicitados');
    } else {
      // Crear un objeto FormData para enviar los datos
      const formData = new FormData();
      formData.append('NumArticulo', this.datosArticulo.NumArticulo);
      formData.append('Descripcion', this.datosArticulo.Descripcion);
      formData.append('Codigo_barras', this.datosArticulo.Codigo_barras);
      formData.append('Fabricante', this.datosArticulo.Fabricante);
      formData.append('Unidad_medida', this.datosArticulo.Unidad_medida);
      formData.append('Categoria_CodigoClasificacion', this.datosArticulo.Categoria_CodigoClasificacion);
      formData.append('Costo', this.datosArticulo.Costo);
      // Agregar la imagen al FormData
      formData.append('imagen', this.datosArticulo.imagen);      
      

      this._serviceM.agregarProducto(formData).subscribe((data: any) => {
        if (data.message == 'Datos del producto insertados exitosamente') {
          alert('Registrado exitosamente');
        } else if (data.message == 'El producto con este NumArticulo ya existe') {
          alert('El producto con este Número de articulo ya existe');
        } else {
          alert('Error al registrar el producto');
        }
      });
    }
  }
  

  onImageSelect(event: any) {
    const file = event.target.files[0];
    if (file) {
      this.datosArticulo.imagen = file;
    } else {
      this.datosArticulo.imagen = '';
    }
  }
  

}

