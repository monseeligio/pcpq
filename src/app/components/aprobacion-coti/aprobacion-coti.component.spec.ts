import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AprobacionCotiComponent } from './aprobacion-coti.component';

describe('AprobacionCotiComponent', () => {
  let component: AprobacionCotiComponent;
  let fixture: ComponentFixture<AprobacionCotiComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AprobacionCotiComponent]
    });
    fixture = TestBed.createComponent(AprobacionCotiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
