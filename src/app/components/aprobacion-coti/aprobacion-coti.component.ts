import { Component } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import { CotizacionService } from 'src/app/services/serviceUsurio/cotizacion.service';
import { Router } from '@angular/router';
import { format } from 'date-fns';
import Swal from 'sweetalert2';




@Component({
  selector: 'app-aprobacion-coti',
  templateUrl: './aprobacion-coti.component.html',
  styleUrls: ['./aprobacion-coti.component.css']
})
export class AprobacionCotiComponent {

  constructor(private _service:UsuarioService, private productosService:CotizacionService, private _router: Router){}
  ngOnInit(): void {
    this.obtener()
    this.obtenerProductos()
  }
  
  datosCotizacion={FechaEmision:'', FechaExpiracion:'', Total:'', Flete:'',Clientes_ID_Cliente:'', Usuario_Clave_usuario:'', Comentarios:'', Status:''}
  public datos:any
  public NumCotizacion:any
  public datos2:any
  public productos:any
  public Clave_usuario:any

  
  obtener() {
    this.datos = this.productosService.ObtenerID();
    this._service.obtenerdatosCoti(this.datos).subscribe((resp: any) => {
      this.datosCotizacion.FechaEmision = resp[0].FechaEmision;
      const fecha = new Date(resp[0].FechaEmision);
      const fechaFormateada = format(fecha, 'dd/MM/yy');
      this.datosCotizacion.FechaEmision=fechaFormateada;
      const fecha2 = new Date(resp[0].FechaExpiracion);
      const fechaFormateada2 = format(fecha2, 'dd/MM/yy');
      this.datosCotizacion.FechaExpiracion = fechaFormateada2;
      this.datosCotizacion.Total = resp[0].Total;
      this.datosCotizacion.Flete = resp[0].Flete;
      this.datosCotizacion.Clientes_ID_Cliente = resp[0].Razon_Social;
      this.datosCotizacion.Usuario_Clave_usuario = resp[0].Nombre;
      this.Clave_usuario= resp[0].Clave_usuario;
      this.datosCotizacion.Comentarios = resp[0].Comentarios;
      this.datosCotizacion.Status = resp[0].Status;
      this.datosCotizacion.Total = resp[0].Total;

      //console.log(resp);
     
    });
  }

  obtenerProductos(){
    this.datos2=this.productosService.ObtenerID()
    this._service.obtenerProductosCotiA(this.datos2).subscribe((resp: any) => {
      this.productos=resp
      //console.log("obtiene productos",this.productos);
    })
  }


  mostrarConfirmacionUpdateA(): Promise<boolean> {
    return Swal.fire({
      title: '¿Estás seguro?',
      text: 'Dar clic en si para aprobar cotización',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, Aprobar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.aprobar();
      }
      return result.isConfirmed;
    });
  }

  
   aprobar(){
    this.datos = {
      id: this.productosService.ObtenerID(),
      Usuario:this.Clave_usuario
    };
    //console.log(this.datos);
    this._service.aprobarCotizacion(this.datos).subscribe((data:any)=>{
      Swal.fire({
        icon: 'success',
        title: 'Aprobación exitosa',
        text: 'Se aprobo correctamente la cotizacion',
      }).then((result) => {
        if (result.isConfirmed) {
          window.location.reload();
        }
    })
  })
  }

  mostrarConfirmacionUpdateR(): Promise<boolean> {
    return Swal.fire({
      title: '¿Estás seguro?',
      text: 'Dar clic en si para rechazar cotización',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, Rechazar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.rechazar();
      }
      return result.isConfirmed;
    });
  }
  
  rechazar(){
    this.datos = {
      id: this.productosService.ObtenerID(),
      Usuario:this.Clave_usuario
    };
    //console.log(this.datos);
    this._service.rechazarCotizacion(this.datos).subscribe((data:any)=>{
      Swal.fire({
        icon: 'warning',
        title: 'Rechazada',
        text: 'Se rechazo la cotizacion',
      })
      window.location.reload();

  })
  }

}


