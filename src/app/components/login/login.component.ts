import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  constructor(private _service:UsuarioService, private _router: Router) { }
  ngOnInit(): void {
    this.role = localStorage.getItem('role');
    //console.log('role '+this.role );
  }
  public role:any;
  datosLogin = {Clave_usuario: '', Password: ''};

  
  login() {
    if (this.datosLogin.Clave_usuario == '' || this.datosLogin.Password == '') {
      alert('Campos vacíos');
    }
    
    else {
    //console.log(this.datosLogin.Clave_usuario, this.datosLogin.Password);
    this._service.login(this.datosLogin).subscribe((resp: any) => {
      //console.log(resp);
        if (resp.Rol=="Administrador") {
          //console.log("aqui", resp);
          localStorage.setItem('rol', 'Administrador');
          localStorage.setItem("usuario", resp.Nombre);
          localStorage.setItem("clave", resp.Clave_usuario);
          localStorage.setItem('token', resp.token);
          this._router.navigate(['menu']);
        }
        if (resp.Rol=="Gerente de ventas") {
          localStorage.setItem('rol', 'Gerente de ventas');
          localStorage.setItem("usuario", resp.Nombre); 
          localStorage.setItem("clave", resp.Clave_usuario);
          localStorage.setItem('token', resp.token);
          this._router.navigate(['menu']);
        }
        if (resp.Rol=="Vendedor") {
          localStorage.setItem('rol', 'Vendedor');
          localStorage.setItem("usuario", resp.Nombre);  
          localStorage.setItem("clave", resp.Clave_usuario);
          localStorage.setItem('token', resp.token);
          this._router.navigate(['menu']);
        }
      },
      (error: any) => {
        const errorMessage = error.error.message; // Obtener el mensaje de error del servidor
        //console.log(errorMessage);
        if (errorMessage == 'Contraseña incorrecta'){
          alert('Password Incorrecta');
        }
  
        if (errorMessage == 'No existe usuario'){
          alert('No existe usuario');
        }
      });
    }
  }
}
