import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductosGComponent } from './productos-g.component';

describe('ProductosGComponent', () => {
  let component: ProductosGComponent;
  let fixture: ComponentFixture<ProductosGComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductosGComponent]
    });
    fixture = TestBed.createComponent(ProductosGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
