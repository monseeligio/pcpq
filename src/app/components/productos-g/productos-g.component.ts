import { Component, ViewChild } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import * as moment from 'moment';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { CotizacionService } from 'src/app/services/serviceUsurio/cotizacion.service';
import { Router } from '@angular/router';
import * as XLSX from 'xlsx';


@Component({
  selector: 'app-productos-g',
  templateUrl: './productos-g.component.html',
  styleUrls: ['./productos-g.component.css']
})
export class ProductosGComponent {
 
  constructor(private _service:UsuarioService, private productosService:CotizacionService, private _router: Router){}
  public cantidad:any
  public role:any

  ngOnInit(): void {
    this.getProductos()
    this.role=localStorage.getItem("rol")
  }
  
  public productos:any;
  datosProductos={Productos_NumArticulo:'', Descripcion:'',Cotizacion_NumCotizacion:'', Cantidad:'', Unidad_Medida:'', Costo:'',Flete: '', Precio:'', VA:'', Desct:'', PrecioUnitario:'', PrecioTotal:'', Margen:''}

  getProductos(){
    this._service.Productos().subscribe((resp:any)=>{
      if (Array.isArray(resp)) { 
        this.productos = resp;
        //console.log(resp);
    }
  })
}

getImageUrl(imageId: any) {
  //console.log("mostrar id",imageId);
  return this._service.getImagen(imageId)
}

  agregarProducto(producto:any) {
    this.datosProductos.Productos_NumArticulo=producto.NumArticulo
    this.datosProductos.Productos_NumArticulo=producto.NumArticulo
    this.datosProductos.Descripcion=producto.Descripcion
    this.datosProductos.Cantidad=producto.Cantidad
    this.datosProductos.Unidad_Medida=producto.Unidad_Medida
    this.datosProductos.Costo=producto.Costo
    if(producto.Cantidad==null || producto.Cantidad=='0'){
      alert("Ingrese la cantidad de producto")
    }
    else{
    this.productosService.agregarProducto(this.datosProductos);
    this.datosProductos={Productos_NumArticulo:'', Descripcion:'',Cotizacion_NumCotizacion:'', Cantidad:'', Unidad_Medida:'', Costo:'' ,Flete: '', Precio:'', VA:'', Desct:'', PrecioUnitario:'', PrecioTotal:'', Margen:''}
    this._router.navigate(['cotizacion']);
    }
  }

  imagenError(event: any) {
    //console.log('Error al cargar la imagen:', event);
  }

  // tu-componente-angular.component.ts
handleFileInput(event: any) {
  const file = event.target.files[0];

  const formData = new FormData();
  formData.append('file', file);

  this._service.saveData(formData).subscribe(
    (resp: any) => {
      //console.log(resp);
    },
    (error: any) => {
      console.error(error);
    }
  );
}

@ViewChild('fileInput') fileInput: any;

uploadFile() {
  const file = this.fileInput.nativeElement.files[0];

  if (file) {
    const formData = new FormData();
    formData.append('file', file);

    this._service.saveData(formData).subscribe(
      (resp: any) => {
        //console.log(resp);
        alert("Costos actualizados");
        
      },
      (error: any) => {
        console.error(error);
        alert("Ocurrio un error, verifica que tu documento se encuentre en formato correcto")
      }
    );
  }
}

validarInput(event: any) {
  const input = event.target;
  input.value = input.value.replace(/[^0-9]/g, ''); // Elimina caracteres no numéricos
}
  
}
