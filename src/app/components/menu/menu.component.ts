import { Component } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {

  constructor(private _service:UsuarioService, private _router: Router){}

  public role:any;
  public usuario:any;
  public numClientes:any;
  public numProductos:any;
  public numCotizaciones:any;
  public NumCotiA:any

  ngOnInit(): void {
   
    this.role = localStorage.getItem('rol');
    this.usuario = localStorage.getItem('usuario')
    //this.usuario = localStorage.getItem('usuario')?.toLowerCase();
    this.getClientes();
    this.getProductos();
    this.getCotizaciones();
    this.getnumeroAprobar()
    
  }



  getClientes(){
    this._service.clientesC().subscribe((resp:any)=>{
     this.numClientes=resp[0]["count(*)"];
     //console.log(resp);
     
    })
  }
  getProductos(){
    this._service.ProductosContar().subscribe((resp:any)=>{
     this.numProductos=resp[0]["count(*)"];
     //console.log(resp);
     
    })
  }

  getCotizaciones(){
    this._service.CotizacionesContar().subscribe((resp:any)=>{
     this.numCotizaciones=resp[0]["count(*)"];
     //console.log("HOLAAA");
     
     //console.log(resp);
    })
  }

  getnumeroAprobar(){
    this.usuario = localStorage.getItem('clave');
    //console.log("USUARIO", this.usuario);
    this._service.obtenerCotizacionesRevisar(this.usuario).subscribe((resp:any)=>{
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.NumCotiA = resp.length;
        //console.log(this.NumCotiA.length);
        
    }
    })
  }



  
}
