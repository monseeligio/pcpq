import { Component,  OnInit, ViewChild, TemplateRef } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-verclientes',
  templateUrl: './verclientes.component.html',
  styleUrls: ['./verclientes.component.css']
})
export class VerclientesComponent {


  @ViewChild("update", {static: false}) myModalInfo!: TemplateRef<any>; 


  public datos:any;
  public status:any;
  public buscarRazonS:any
  public role:any
  public usuario:any
  datosUsuario={ID_Cliente:'', Razon_Social:'', RFC:'', Domicilio_Fiscal:'', email:'', telefono_empresarial:''}
  datosUsuarioU={ID_Cliente:'', Razon_Social:'', RFC:'', Domicilio_Fiscal:'', email:'', telefono_empresarial:''}

  constructor(private _serviceM:UsuarioService, private modalService: NgbModal){}
  
  ngOnInit(): void {
    this.getRol()
    this.role = localStorage.getItem('rol');
    this.usuario = localStorage.getItem('usuario')

  }

  mostrarModalInfo(id:any){
  this.modalService.open(this.myModalInfo);
  }
  
  mostrarConfirmacionEliminar1(id:any): Promise<boolean> {
    return Swal.fire({
      title: '¿Estás seguro?',
      text: 'No podrás revertir esto',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, borrarlo'
    }).then((result) => {
      if (result.isConfirmed) {
        this.EliminarRol(id);
      }
      return result.isConfirmed;
    });
  }

  mostrarModalUptade1(id:any){
    this.modalService.open(this.myModalInfo);
    this.datosUsuarioU.ID_Cliente=id.ID_Cliente
    this.datosUsuarioU.Razon_Social=id.Razon_Social
    this.datosUsuarioU.RFC=id.RFC
    this.datosUsuarioU.Domicilio_Fiscal=id.Domicilio_Fiscal
    this.datosUsuarioU.email=id.email 
    this.datosUsuarioU.telefono_empresarial=id.telefono_empresarial    
  }


  getRol(){
    this._serviceM.getCliente().subscribe((resp:any)=>{
      //console.log(resp);
      if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
        this.datos = resp;
    }
  })
  this.buscarRazonS=''
  }


  EliminarRol(id: string){    
    this._serviceM.DeleteCliente(id).subscribe((data:any)=>{
      this.getRol();
    }) 
  }



  mostrarConfirmacionUpdate(): Promise<boolean> {
    return Swal.fire({
      title: '¿Estás seguro?',
      text: 'Dar clic en si para actualizar registro',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, Actualizar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.actualizar();
      }
      return result.isConfirmed;
    });
  }



  actualizar(){
    console.log(this.datosUsuarioU);
    if(this.datosUsuarioU.Razon_Social==''){
      alert("Por favor, ingresa el nombre del cliente")
    }
    else if (this.datosUsuarioU.RFC && !this.validarRFC(this.datosUsuarioU.RFC)) {
      window.alert('RFC inválido. Por favor, ingresa un RFC válido.');
      }
    else if (this.datosUsuarioU.email && !this.validarCorreo(this.datosUsuarioU.email)) {
      window.alert('Correo inválido. Por favor, ingresa un correo válido.');
    }
    else{
    this._serviceM.UpdateCliente(this.datosUsuarioU).subscribe((data:any)=>{
      this.getRol()
      this.datosUsuarioU={ID_Cliente:'', Razon_Social:'', RFC:'', Domicilio_Fiscal:'', email:'', telefono_empresarial:''}
      this.modalService.dismissAll()
    })
    }
  }

  validarRFC(rfc: string): boolean | undefined {
    // Verificar si rfc tiene un valor definido y no es null ni undefined
    if (rfc != null && typeof rfc === 'string') {
      // Expresión regular para validar RFC
      const rfcRegex = /^[A-Z&Ñ]{3,4}\d{6}[A-Z\d]{3}$/;
      return rfc.match(rfcRegex) !== null;
    }
    // No hacer nada si rfc no se ha proporcionado
    return undefined;
  }
  
  
  validarCorreo(email:any) {
    const regexCorreo = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
    return regexCorreo.test(email);
  }



    buscarCliente(){
      this._serviceM.buscarCliente(this.buscarRazonS).subscribe((resp:any)=>{
        //console.log(resp);
        if (Array.isArray(resp)) { // Comprobar si resp es un arreglo
          this.datos = resp;
      }
    })
    }

    cerrar(){
      this.modalService.dismissAll()
    }

    

}
