import { Component } from '@angular/core';
import { UsuarioService } from 'src/app/services/serviceUsurio/usuario.service';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-nuevo-usuario',
  templateUrl: './nuevo-usuario.component.html',
  styleUrls: ['./nuevo-usuario.component.css']
})
export class NuevoUsuarioComponent {
  constructor(private _serviceM:UsuarioService, private sanitizer: DomSanitizer){}

  datosUsuario={Clave_usuario:'', Nombre:'', ApellidoP:'', ApellidoM:'', Telefono:'' ,Rol:'Seleccione', email:'', Status:'Seleccione', Password:'', imagen: ''}
  public confirmarContra:any

  ngOnInit(): void {
  }

  rolSeleccionado: string = 'Seleccione'; 

  AgregarUser(){
    if(this.datosUsuario.Clave_usuario=='' || this.datosUsuario.Nombre=='' || this.datosUsuario.ApellidoP=='' || this.datosUsuario.Telefono=='' || this.datosUsuario.Rol=='Seleccione' || this.datosUsuario.email=='' || this.datosUsuario.Status=='Seleccione' || this.datosUsuario.Password=='' ){
      window.alert("Debes ingresar todos los datos solicitados")
     
    }
    else{
      if (this.datosUsuario.Rol=="Vendedor" && this.datosUsuario.imagen==''){
        window.alert("Debes ingresar todos los datos solicitados")
      }
      else if (!this.validarCorreo(this.datosUsuario.email)) {
        alert('Correo no inválido. Por favor, ingresa un correo válido.');
      }
      else{
      const formData = new FormData();
      formData.append('Clave_usuario', this.datosUsuario.Clave_usuario);
      formData.append('Nombre', this.datosUsuario.Nombre);
      formData.append('ApellidoP', this.datosUsuario.ApellidoP);
      formData.append('ApellidoM', this.datosUsuario.ApellidoM);
      formData.append('Rol', this.datosUsuario.Rol);
      formData.append('email', this.datosUsuario.email);
      formData.append('Status', this.datosUsuario.Status);
      formData.append('Password', this.datosUsuario.Password);
      formData.append('Telefono', this.datosUsuario.Telefono);
      formData.append('imagen', this.datosUsuario.imagen)     
     this._serviceM.agregarUsuario(formData).subscribe((data:any)=>{//LLAMA AL SERVICIO PARA AGREGAR AL USUARIO
      //console.log(data.mensaje);
      
      if (data.message == 'Datos insertados exitosamente') {
        alert("Registrado exitosamente");
      } 
      else if(data.mensaje == 'Debes ingresar correctamente los datos solicitados') {
        alert("La contraseña debe contener al menos 8 caracteres");
      }
      else if(data.mensaje == 'El usuario ya se encuentra registrado') {
        alert("El usuario ya se encuentra registrado");
      }
      else if(data.mensaje == 'El correo electrónico ya se encuentra registrado') {
        alert("El correo electrónico ya se encuentra registrado");
      }

    })}
  }
  }

  
  onImageSelect(event: any) {
    const file = event.target.files[0];
    if (file) {
      this.datosUsuario.imagen = file;
    } else {
      this.datosUsuario.imagen = '';
    }
  }
  
  
  validarCorreo(email:any) {
    const regexCorreo = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
    return regexCorreo.test(email);
  }

}
