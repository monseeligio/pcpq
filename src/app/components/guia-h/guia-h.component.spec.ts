import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuiaHComponent } from './guia-h.component';

describe('GuiaHComponent', () => {
  let component: GuiaHComponent;
  let fixture: ComponentFixture<GuiaHComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GuiaHComponent]
    });
    fixture = TestBed.createComponent(GuiaHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
