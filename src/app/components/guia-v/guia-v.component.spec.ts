import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuiaVComponent } from './guia-v.component';

describe('GuiaVComponent', () => {
  let component: GuiaVComponent;
  let fixture: ComponentFixture<GuiaVComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GuiaVComponent]
    });
    fixture = TestBed.createComponent(GuiaVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
